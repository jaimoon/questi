package com.versatilemobitech.questi.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AskQuestionActivity extends BaseActivity implements View.OnClickListener, IParseListener {
    private TextView mTxtUserName, mTxtTitle, mTxtAsk;
    private EditText mEdtQuestion;
    private ImageView mImgUser, mImgBack;
    private String userId = "", profilePic = "";
    private int badCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_question);

        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
        getIntentData();
    }

    private void setReferences() {
        mTxtUserName = (TextView) findViewById(R.id.txtUserName);
        mEdtQuestion = (EditText) findViewById(R.id.txtQuestion);

        mTxtTitle = (TextView) findViewById(R.id.txtTitle);
        mTxtTitle.setText(getString(R.string.ask_question));

        mTxtAsk = (TextView) findViewById(R.id.txtAskkkk);
        mImgUser = (ImageView) findViewById(R.id.imgUser);
        mImgBack = (ImageView) findViewById(R.id.imgBack);
    }

    private void setClickListeners() {
        mTxtAsk.setOnClickListener(this);
        mImgBack.setOnClickListener(this);
    }

    private void getIntentData() {
        if (getIntent() != null) {
            userId = getIntent().getStringExtra("USERID");
            mTxtUserName.setText(getIntent().getStringExtra("USERNAME"));

            profilePic = getIntent().getStringExtra("PROFILEPIC");
            if (!profilePic.equalsIgnoreCase("")) {
                Picasso.with(this).load(profilePic).error(R.drawable.user_placeholder).placeholder(R.drawable.user_placeholder).into(mImgUser);
            } else {
                Picasso.with(this).load(R.drawable.user_placeholder).into(mImgUser);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.txtAskkkk:
                if (!checkValidation().equalsIgnoreCase("")) {
                    PopUtils.alertDialog(this, checkValidation(), null);
                } else {
                    if (PopUtils.checkInternetConnection(this)) {
                        requestForAskQuestionWS();
                    } else {
                        PopUtils.alertDialog(this, getString(R.string.pls_check_internet), null);
                    }
                }
                break;
            default:
                break;
        }
    }

    public String checkValidation() {
        ArrayList<String> mBanWords = new ArrayList<>();
        mBanWords.add("shit");
        mBanWords.add("fuck");
        mBanWords.add("bitch");
        mBanWords.add("piss");
        mBanWords.add("dick");
        mBanWords.add("cock");
        mBanWords.add("fag");
        mBanWords.add("pussy");
        mBanWords.add("asshole");
        mBanWords.add("bastard");
        mBanWords.add("slut");
        mBanWords.add("fat");
        mBanWords.add("douche");
        mBanWords.add("bloody");
        mBanWords.add("portly");
        mBanWords.add("bugger");
        mBanWords.add("arsehole");
        mBanWords.add("ugly");
        mBanWords.add("nigga");
        mBanWords.add("overweight");
        mBanWords.add("hell");
        mBanWords.add("whore");
        mBanWords.add("puta");
        mBanWords.add("tit");
        mBanWords.add("tits");
        mBanWords.add("retard");
        mBanWords.add("beggar");
        mBanWords.add("gay");
        mBanWords.add("obese");
        mBanWords.add("fugly");
        mBanWords.add("twat");
        mBanWords.add("thot");
        mBanWords.add("wanker");
        mBanWords.add("cumdumpster");
        mBanWords.add("fuckface");
        mBanWords.add("shite");
        mBanWords.add("nutsack");
        mBanWords.add("arsewipe");
        mBanWords.add("asswipe");
        mBanWords.add("clusterfuck");

        String message = "";
        if (mEdtQuestion.getText().toString().trim().length() == 0) {
            message = getString(R.string.please_ask_any_question_to_ur_friend);
        } else {
            String s = mEdtQuestion.getText().toString().trim();
            String[] words = s.split("\\W+");

            for (int i = 0; i < mBanWords.size(); i++) {
                String mBanWord = mBanWords.get(i);
                for (int j = 0; j < words.length; j++) {
                    if (words[j].matches(mBanWord)) {
                        message = getString(R.string.bad_words_alert_for_question);
                    }
                }
            }
        }
        return message;
    }

    private void requestForAskQuestionWS() {
        showLoadingDialog(getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("question", mEdtQuestion.getText().toString());
        params.put("userid", UserDetails.getInstance(this).getUserId());
        params.put("receiver_id", userId);
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_ASK_QUESTION, params, this, WsUtils.WS_CODE_ASK_QUESTION);
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        hideLoadingDialog();
        PopUtils.alertDialog(this, getString(R.string.something_wrong), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        hideLoadingDialog();
        switch (requestCode) {
            case WsUtils.WS_CODE_ASK_QUESTION:
                responseForAskQuestion(response);
                break;
            default:
                break;
        }
    }

    private void responseForAskQuestion(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    PopUtils.alertDialog(this, message, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onBackPressed();
                        }
                    });
                } else {
                    PopUtils.alertDialog(this, message, null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
