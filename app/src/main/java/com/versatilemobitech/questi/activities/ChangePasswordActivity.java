package com.versatilemobitech.questi.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Excentd11 on 8/23/2017.
 */

public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener, IParseListener {
    private TextView mTxtSubmit, mTxtTitle;
    private EditText mEdtCurrentPwd, mEdtNewPwd, mEdtReNewPwd;
    private ImageView mImgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
    }


    private void setReferences() {
        mTxtTitle = (TextView) findViewById(R.id.txtTitle);
        mTxtTitle.setText("Change Password");

        mTxtSubmit = (TextView) findViewById(R.id.txtSubmit);
        mEdtCurrentPwd = (EditText) findViewById(R.id.edtCurrentPwd);
        mEdtNewPwd = (EditText) findViewById(R.id.edtNewPwd);
        mEdtReNewPwd = (EditText) findViewById(R.id.edtReNewPwd);
        mImgBack = (ImageView) findViewById(R.id.imgBack);
    }

    private void setClickListeners() {
        mTxtSubmit.setOnClickListener(this);
        mImgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSubmit:
                if (!checkValidation().equalsIgnoreCase("")) {
                    PopUtils.alertDialog(this, checkValidation(), null);
                } else {
                    if (PopUtils.checkInternetConnection(this)) {
                        requestForChangePasswordWS();
                    } else {
                        PopUtils.alertDialog(this, getString(R.string.pls_check_internet), null);
                    }
                }
                break;
            case R.id.imgBack:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    public String checkValidation() {
        String message = "";
        if (mEdtCurrentPwd.getText().toString().trim().length() == 0) {
            message = "Please enter Old Password.";
        } else if (mEdtCurrentPwd.getText().toString().trim().length() < 6) {
            message = "Entered Password should be minimum 6 letters.";
        } else if (mEdtNewPwd.getText().toString().trim().length() == 0) {
            message = "Please enter New Password.";
        } else if (mEdtNewPwd.getText().toString().trim().length() < 6) {
            message = "Entered New Password should be minimum 6 letters.";
        } else if (mEdtCurrentPwd.getText().toString().equalsIgnoreCase(mEdtNewPwd.getText().toString())) {
            message = "New Password should not be same as Old Password.";
        } else if (mEdtReNewPwd.getText().toString().trim().length() == 0) {
            message = "Please confirm New Password.";
        } else if (!mEdtReNewPwd.getText().toString().trim().equalsIgnoreCase(mEdtNewPwd.getText().toString().trim())) {
            message = "Confirm Password should be same as New Password.";
        }
        return message;
    }

    private void requestForChangePasswordWS() {
        showLoadingDialog("Loading", false);
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(this).getUserId());
        params.put("oldpassword", mEdtCurrentPwd.getText().toString().trim());
        params.put("newpassword", mEdtReNewPwd.getText().toString());
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_MY_CHANGE_PASSWORD, params, this, WsUtils.WS_CODE_CHANGE_PASSWORD);
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        hideLoadingDialog();
        PopUtils.alertDialog(this, getString(R.string.something_wrong), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        hideLoadingDialog();
        switch (requestCode) {
            case WsUtils.WS_CODE_CHANGE_PASSWORD:
                responseForChangePassword(response);
                break;
            default:
                break;
        }
    }

    private void responseForChangePassword(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    PopUtils.alertDialog(this, message, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            UserDetails.getInstance(ChangePasswordActivity.this).clear();
                            UserDetails.getInstance(ChangePasswordActivity.this).setUserId("");
                            startActivity(new Intent(ChangePasswordActivity.this, LoginActivity.class));
                            finishAffinity();
                        }
                    });
                } else {
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

