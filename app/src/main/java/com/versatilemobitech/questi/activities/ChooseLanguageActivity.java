package com.versatilemobitech.questi.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.interfaces.DialogListInterface;
import com.versatilemobitech.questi.models.DialogList;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;

import java.util.ArrayList;

public class ChooseLanguageActivity extends BaseActivity implements View.OnClickListener {
    private TextView mTxtLanguage, mTxtContinue;
    private ArrayList<DialogList> mDialogList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_language);

        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
    }

    private void setReferences() {
        mTxtLanguage = (TextView) findViewById(R.id.txtLanguage);
        mTxtContinue = (TextView) findViewById(R.id.txtContinue);
    }

    private void setClickListeners() {
        mTxtLanguage.setOnClickListener(this);
        mTxtContinue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtContinue:
                navigateActivity(new Intent(this, LoginActivity.class), true);
                break;
            case R.id.txtLanguage:
//                showLanguageDialog();
                break;
            default:
                break;
        }
    }

    private void showLanguageDialog() {
        mDialogList.clear();
        mDialogList.add(new DialogList("", "ENGLISH"));
        mDialogList.add(new DialogList("", "HINDI"));
        mDialogList.add(new DialogList("", "SPANISH"));
        PopUtils.alertDialogList(this, mDialogList, new DialogListInterface() {
            @Override
            public void DialogListInterface(String id, String value) {
                mTxtLanguage.setText(value);
                UserDetails.getInstance(ChooseLanguageActivity.this).setLanguage(value);
                PopUtils.SetLocalization(ChooseLanguageActivity.this, UserDetails.getInstance(ChooseLanguageActivity.this).getLanguage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        ChooseLanguageActivity.this.finishAffinity();
    }
}
