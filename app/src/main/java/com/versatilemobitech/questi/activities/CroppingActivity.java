package com.versatilemobitech.questi.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;

import com.isseiaoki.simplecropview.CropImageView;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.fragments.AnswerFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * Created by Excentd11 on 8/30/2017.
 */

public class CroppingActivity extends BaseActivity implements View.OnClickListener {
    private CropImageView mCropImageView;
    private ImageView mImgDone, mImgRotate;
    private String from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cropping);

        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
        getBundleData();
    }

    private void getBundleData() {
        Bundle bundle = getIntent().getExtras();
        final String imagePath = bundle.getString("image_path");
        from = bundle.getString("FROM");

        if (from.equalsIgnoreCase("HOWYOULOOK")) {
            mCropImageView.setCropMode(CropImageView.CropMode.CIRCLE);
        }

        Bitmap mBitmap = getRotatedBitmap(0, imagePath);
        mCropImageView.setImageBitmap(mBitmap);

    }

    private void setReferences() {
        mCropImageView = (CropImageView) findViewById(R.id.cropImageView);
        mImgDone = (ImageView) findViewById(R.id.imgDone);
        mImgRotate = (ImageView) findViewById(R.id.imgRotate);
    }

    private void setClickListeners() {
        mImgDone.setOnClickListener(this);
        mImgRotate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgDone:
                Bitmap bitmap = mCropImageView.getCroppedBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, baos);
                byte[] b = baos.toByteArray();
                String temp = Base64.encodeToString(b, Base64.DEFAULT);

                if (from.equalsIgnoreCase("HOWYOULOOK")) {
                    HowYouLookActivity.getInstance().cropImageInterface(bitmap, temp);
                } else if (from.equalsIgnoreCase("ANSWER")) {
                    AnswerFragment.getInstance().cropImageInterface(bitmap, temp);
                }

                finish();
                break;
            case R.id.imgRotate:
                mCropImageView.rotateImage(CropImageView.RotateDegrees.ROTATE_M90D);
                break;
            default:
                break;
        }
    }

    public static Bitmap getRotatedBitmap(int rotation, String mPath) {
        File f = new File(mPath);
        Bitmap mBitMap = BitmapFactory.decodeFile(f.getAbsolutePath());

        int width = mBitMap.getWidth();
        int height = mBitMap.getHeight();

        /*ULTRA HEIGHT RESOLUTION IMAGE */
        if (width > 1920 && height > 1080) {
            mBitMap = Bitmap.createScaledBitmap(mBitMap, width, height, false);

           /* if (width > height)
                mBitMap = Bitmap.createScaledBitmap(mBitMap, width, height, false);
//              mBitMap = Bitmap.createScaledBitmap(mBitMap, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, false);

            else
//              mBitMap = Bitmap.createScaledBitmap(mBitMap, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, false);
                mBitMap = Bitmap.createScaledBitmap(mBitMap, width, height, false);*/

            Bitmap oldBitmap = mBitMap;
            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);
            mBitMap = Bitmap.createBitmap(oldBitmap, 0, 0, oldBitmap.getWidth(), oldBitmap.getHeight(), matrix, false);
        } else if (rotation != 0) {
            Bitmap oldBitmap = mBitMap;
            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);
            mBitMap = Bitmap.createBitmap(oldBitmap, 0, 0, oldBitmap.getWidth(), oldBitmap.getHeight(), matrix, false);
            oldBitmap.recycle();
        }
        return mBitMap;
    }
}
