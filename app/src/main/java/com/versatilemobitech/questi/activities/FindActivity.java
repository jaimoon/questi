package com.versatilemobitech.questi.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.versatilemobitech.questi.R;

public class FindActivity extends BaseActivity implements View.OnClickListener {
    private TextView mTxtFacebook, mTxtSkip, mTxtTitle;
    private ImageView mImgBack;
    private int backClick = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);
        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
    }

    private void setReferences() {
        mTxtFacebook = (TextView) findViewById(R.id.txtFacebook);
        mTxtSkip = (TextView) findViewById(R.id.txtSkip);
        mTxtTitle = (TextView) findViewById(R.id.txtTitle);
        mTxtTitle.setText(getString(R.string.find_friends));

        mImgBack = (ImageView) findViewById(R.id.imgBack);
    }

    private void setClickListeners() {
        mTxtFacebook.setOnClickListener(this);
        mTxtSkip.setOnClickListener(this);
        mImgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtFacebook:
                break;
            case R.id.txtSkip:
                navigateActivity(new Intent(this, HomeActivity.class), true);
                break;
            case R.id.imgBack:
                setBackClicked();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        setBackClicked();
    }

    private void setBackClicked() {
        if (backClick >= 1) {
            finish();
        } else {
            Toast.makeText(this, getString(R.string.please_click_again_for_exit), Toast.LENGTH_SHORT).show();
            backClick = backClick + 1;
        }
    }
}

