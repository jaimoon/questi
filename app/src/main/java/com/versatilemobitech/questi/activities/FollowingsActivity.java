package com.versatilemobitech.questi.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.adapters.FollowingsAdapter;
import com.versatilemobitech.questi.adapters.UserQuestionsAdapter;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.models.Followings;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Excentd11 on 8/24/2017.
 */

public class FollowingsActivity extends BaseActivity implements View.OnClickListener, IParseListener {
    public RecyclerView mRecyclerView;
    private TextView mTxtContent, mTxtTitle;
    private ImageView mImgBack;
    private String profileId = "";
    private ArrayList<Followings> mFollowingsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followings);

        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
        getIntentData();

        if (PopUtils.checkInternetConnection(this)) {
            requestForFollowersWS();
        } else {
            PopUtils.alertDialog(this, getString(R.string.pls_check_internet), null);
        }
    }

    private void setReferences() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mTxtContent = (TextView) findViewById(R.id.txtContent);
        mTxtTitle = (TextView) findViewById(R.id.txtTitle);
        mTxtTitle.setText(getString(R.string.followings));

        mImgBack = (ImageView) findViewById(R.id.imgBack);
    }

    private void setClickListeners() {
        mImgBack.setOnClickListener(this);
    }

    private void getIntentData() {
        if (getIntent() != null) {
            profileId = getIntent().getStringExtra("USERID");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    private void requestForFollowersWS() {
        showLoadingDialog(getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(this).getUserId());
        params.put("profileid", profileId);
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_FOLLOWINGS, params, this, WsUtils.WS_CODE_FOLLOWINGS);
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        hideLoadingDialog();
        PopUtils.alertDialog(this, getString(R.string.something_wrong), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        hideLoadingDialog();
        switch (requestCode) {
            case WsUtils.WS_CODE_FOLLOWINGS:
                responseForFollowings(response);
                break;
            default:
                break;
        }
    }

    private void responseForFollowings(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mTxtContent.setVisibility(View.GONE);

                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    for (int i = 0; i < mDataArray.length(); i++) {
                        JSONObject mDataObject = mDataArray.getJSONObject(i);

                        mFollowingsList.add(new Followings(mDataObject.optInt("userid"), mDataObject.optString("Username"),
                                mDataObject.optString("profile")));
                    }

                    setAdapter();
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    mTxtContent.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        FollowingsAdapter mFollowingsAdapter = new FollowingsAdapter(this, mFollowingsList);
        mRecyclerView.setAdapter(mFollowingsAdapter);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addOnItemTouchListener(new UserQuestionsAdapter(this, new UserQuestionsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }
        }));
    }
}
