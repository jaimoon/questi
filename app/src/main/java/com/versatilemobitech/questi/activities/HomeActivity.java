package com.versatilemobitech.questi.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.fragments.ExploreFragment;
import com.versatilemobitech.questi.fragments.HomeFragment;
import com.versatilemobitech.questi.fragments.NotificationFragment;
import com.versatilemobitech.questi.fragments.QuestionsFragment;
import com.versatilemobitech.questi.fragments.SearchFragment;
import com.versatilemobitech.questi.fragments.SettingsFragment;
import com.versatilemobitech.questi.helper.BottomNavigationViewHelper;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.views.BadgeView;

public class HomeActivity extends FragmentActivity implements View.OnClickListener {
    public BottomNavigationView mBottomNavigationView;
    public ImageView mImgNotification, mImgLogo, mImgBack;
    public EditText mEdtSearch;
    public TextView mTxtTitle;
    public Button btn_search;
    public LinearLayout mLlProgressBar;
    public BottomNavigationMenuView menuView;
    private BadgeView mBadgeView, mNotificationBadgeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
        setHomeFragment();
        setBottomNavigationView();
    }

    private void setReferences() {
        mBadgeView = new BadgeView(this);
        mNotificationBadgeView = new BadgeView(this);

        mBottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigation);
        BottomNavigationViewHelper.disableShiftMode(mBottomNavigationView);

        mImgNotification = (ImageView) findViewById(R.id.imgNotification);
        mImgLogo = (ImageView) findViewById(R.id.imgLogo);
        mImgBack = (ImageView) findViewById(R.id.imgBack);
        mEdtSearch = (EditText) findViewById(R.id.edtSearch);
        btn_search = (Button) findViewById(R.id.btn_search);
        mTxtTitle = (TextView) findViewById(R.id.txtTitle);
        mLlProgressBar = (LinearLayout) findViewById(R.id.llProgressBar);
    }

    private void setClickListeners() {
        mImgNotification.setOnClickListener(this);
        mImgLogo.setOnClickListener(this);
    }

    private void setBottomNavigationView() {
        /*For Custom Design*/
        menuView = (BottomNavigationMenuView) mBottomNavigationView.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            final View iconView = menuView.getChildAt(i).findViewById(android.support.design.R.id.icon);
            final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
            final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, displayMetrics);
            layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, displayMetrics);
            iconView.setLayoutParams(layoutParams);
        }

        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                BottomNavigationViewHelper.disableShiftMode(mBottomNavigationView);
                if (menuItem.isChecked()) {
                    return true;
                } else {
                    uncheckAllMenus();
                    switch (menuItem.getItemId()) {
                        case R.id.itemHome:
                            menuItem.setChecked(true);
                            setHomeFragment();
                            return true;
                        case R.id.itemExplore:
                            menuItem.setChecked(true);
                            setExploreFragment();
                            return true;
                        case R.id.itemQuestions:
                            menuItem.setChecked(true);
                            setQuestionsFragment();
                            return true;
                        case R.id.itemSearch:
                            menuItem.setChecked(true);
                            setSearchFragment();
                            return true;
                        case R.id.itemSettings:
                            menuItem.setChecked(true);
                            setSettingsFragment();
                            return true;
                        default:
                            Toast.makeText(getApplicationContext(), R.string.something_wrong, Toast.LENGTH_SHORT).show();
                            return true;
                    }
                }
            }

            private void uncheckAllMenus() {
                for (int i = 0; i < mBottomNavigationView.getMenu().size(); i++) {
                    mBottomNavigationView.getMenu().getItem(i).setChecked(false);
                }
            }
        });
    }

    public void setBadgeCount(int count) {
        if (menuView != null) {
            final View iconView = menuView.getChildAt(2).findViewById(android.support.design.R.id.icon);
            mBadgeView.setTargetView(iconView);
            mBadgeView.setBadgeCount(count);
            mBadgeView.setBadgeMargin(15, 5, 15, 5);
        }
    }

    public void setNotificationBadgeCount(int count) {
        mNotificationBadgeView.setTargetView(mImgNotification);
        mNotificationBadgeView.setBadgeCount(count);
        mNotificationBadgeView.setBadgeMargin(15, 0, 15, 15);
    }

    private void setHomeFragment() {
        HomeFragment mHomeFragment = HomeFragment.newInstance();
        Bundle mBundle = new Bundle();
        navigateFragment(mHomeFragment, "HomeFragment", mBundle, this);
    }

    private void setExploreFragment() {
        ExploreFragment mExploreFragment = ExploreFragment.newInstance();
        Bundle mBundle = new Bundle();
        navigateFragment(mExploreFragment, "ExploreFragment", mBundle, this);
    }

    private void setQuestionsFragment() {
        QuestionsFragment mQuestionsFragment = QuestionsFragment.newInstance();
        Bundle mBundle = new Bundle();
        navigateFragment(mQuestionsFragment, "QuestionsFragment", mBundle, this);
    }

    private void setSearchFragment() {
        SearchFragment mSearchFragment = SearchFragment.newInstance();
        Bundle mBundle = new Bundle();
        navigateFragment(mSearchFragment, "SearchFragment", mBundle, this);
    }

    private void setSettingsFragment() {
        SettingsFragment mSettingsFragment = SettingsFragment.newInstance();
        Bundle mBundle = new Bundle();
        navigateFragment(mSettingsFragment, "SettingsFragment", mBundle, this);
    }

    private void setNotificationsFragment() {
        NotificationFragment mNotificationFragment = NotificationFragment.newInstance();
        Bundle mBundle = new Bundle();
        navigateFragment(mNotificationFragment, "NotificationFragment", mBundle, this);
    }

    private void setProfileActivity() {
        Intent i = new Intent(this, MyProfileActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgNotification:
                setNotificationsFragment();
                break;
            case R.id.imgLogo:
                setProfileActivity();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1);
            String tagName = backEntry.getName();
            if (tagName.equals("HomeFragment")) {
                PopUtils.twoButtonDialog(this, getString(R.string.are_u_sure_u_want_to_exit), getString(R.string.yes),
                        getString(R.string.no), yesClick, null);
            } else if (tagName.equals("ExploreFragment")) {
                setHomeFragment();
            } else if (tagName.equals("QuestionsFragment")) {
                setHomeFragment();
            } else if (tagName.equals("SearchFragment")) {
                setHomeFragment();
            } else if (tagName.equals("SettingsFragment")) {
                setHomeFragment();
            } else {
                super.onBackPressed();
            }
        }
    }

    View.OnClickListener yesClick = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onClick(View view) {
            HomeActivity.this.finishAffinity();
        }
    };

    public static void navigateFragment(Fragment fragment, String tag, Bundle bundle, FragmentActivity fragmentActivity) {
        FragmentManager fragmentManager = fragmentActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        fragmentTransaction.replace(R.id.container, fragment, tag);
        if (tag != null) {
            fragmentTransaction.addToBackStack(tag);
        }
        fragmentTransaction.commit();
    }
}

