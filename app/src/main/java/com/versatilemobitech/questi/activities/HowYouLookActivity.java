package com.versatilemobitech.questi.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.util.Base64;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.interfaces.CropImageInterface;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class HowYouLookActivity extends BaseActivity implements View.OnClickListener, IParseListener, CropImageInterface {
    private LinearLayout mLlCb;
    private TextView mTxtTerms, mTxtTitle, mTxtSend;
    private ImageView mImgUser, mImgBack;
    private EditText mEdtUserName;
    private CheckBox mCbTerms;
    private String profileImageString = "", fromscreen = "";
    private int backClick = 0;
    String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};


    private static CropImageInterface mCropImageInterface;

    public static CropImageInterface getInstance() {
        return mCropImageInterface;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_you_look);

        mCropImageInterface = this;
        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
        getIntentData();
    }

    private void setReferences() {
        mLlCb = (LinearLayout) findViewById(R.id.llCheckBox);
        mTxtTerms = (TextView) findViewById(R.id.txtTerms);
        mTxtSend = (TextView) findViewById(R.id.txtSend);
        mTxtTitle = (TextView) findViewById(R.id.txtTitle);
        mTxtTitle.setText(getString(R.string.how_u_look));

        mImgUser = (ImageView) findViewById(R.id.imgUser);
        mImgBack = (ImageView) findViewById(R.id.imgBack);
        mEdtUserName = (EditText) findViewById(R.id.edtUserName);

        mCbTerms = (CheckBox) findViewById(R.id.cbTerms);
    }

    private void setClickListeners() {
        mImgUser.setOnClickListener(this);
        mTxtTerms.setOnClickListener(this);
        mTxtSend.setOnClickListener(this);
        mCbTerms.setOnClickListener(this);
        mImgBack.setOnClickListener(this);
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fromscreen = bundle.getString("FROM");
            if (fromscreen.equalsIgnoreCase("LOGIN")) {
                mLlCb.setVisibility(View.VISIBLE);
                mTxtSend.setVisibility(View.GONE);
                mTxtSend.setText(getString(R.string.send_));
            } else if (fromscreen.equalsIgnoreCase("SETTINGS")) {
                if (UserDetails.getInstance(this).getUserProfileImage().equalsIgnoreCase("")) {
                    Picasso.with(this).load(R.drawable.user_placeholder).into(mImgUser);
                } else {
                    Picasso.with(this).load(UserDetails.getInstance(this).getUserProfileImage()).placeholder(R.drawable.profile_placeholder).into(mImgUser);
                }
                new DownloadFilesTask().execute(UserDetails.getInstance(this).getUserProfileImage());

                mEdtUserName.setText(UserDetails.getInstance(this).getUserName());
                mEdtUserName.setSelection(mEdtUserName.getText().toString().trim().length());

                mTxtSend.setVisibility(View.VISIBLE);
                mTxtSend.setText(getString(R.string.update_));

                mLlCb.setVisibility(View.GONE);
            }
        }
    }

    private class DownloadFilesTask extends AsyncTask<String, Integer, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap myBitmap = null;
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                myBitmap = BitmapFactory.decodeStream(input);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return myBitmap;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                profileImageString = Base64.encodeToString(PopUtils.getBytesFromBitmap(result), Base64.NO_WRAP);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cbTerms:
                if (!checkValidation().equalsIgnoreCase("")) {
                    PopUtils.alertDialog(this, checkValidation(), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mCbTerms.setChecked(false);
                        }
                    });
                } else {
                    if (PopUtils.checkInternetConnection(this)) {
                        requestForAddProfileWS();
                    } else {
                        PopUtils.alertDialog(this, getString(R.string.pls_check_internet), null);
                    }
                }
                break;
            case R.id.txtTerms:
                navigateActivity(new Intent(this, TermsActivity.class), false);
                break;
            case R.id.imgUser:
                if (!PopUtils.hasPermissions(this, PERMISSIONS)) {
                    requestPermissions(PERMISSIONS, 1);
                } else {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, 690);
                }
                break;
            case R.id.imgBack:
                setBackClicked();
                break;
            case R.id.txtSend:
                if (PopUtils.checkInternetConnection(this)) {
                    requestForEditProfileWS();
                } else {
                    PopUtils.alertDialog(this, getString(R.string.pls_check_internet), null);
                }
                break;
            default:
                break;
        }
    }

    private void requestForAddProfileWS() {
        showLoadingDialog(getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("username", mEdtUserName.getText().toString());
        params.put("userid", UserDetails.getInstance(this).getUserId());
        params.put("profile", profileImageString);
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_ADD_PROFILE, params, this, WsUtils.WS_CODE_ADD_PROFILE);
    }

    private String checkValidation() {
        String message = "";
        if (profileImageString.length() == 0) {
            message = getString(R.string.please_select_profile_picture);
        } else if (mEdtUserName.getText().toString().trim().length() == 0) {
            message = getString(R.string.please_enter_username);
        }
        return message;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, 690);
                } else {
                    Toast.makeText(this, getString(R.string.permission_denied_for_gallery), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 690:
                if (data != null) {
                    Uri selectedImageUri = data.getData();
                    try {
                        Intent intent = new Intent(HowYouLookActivity.this, CroppingActivity.class);
                        intent.putExtra("image_path", PopUtils.getRealPathFromURI(this, selectedImageUri));
                        intent.putExtra("FROM", "HOWYOULOOK");
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void requestForEditProfileWS() {
        showLoadingDialog(getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("username", mEdtUserName.getText().toString().trim());
        params.put("userid", UserDetails.getInstance(this).getUserId());
        params.put("profile", profileImageString);
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_EDIT_PROFILE, params, this, WsUtils.WS_CODE_EDIT_PROFILE);
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        hideLoadingDialog();
        PopUtils.alertDialog(this, getString(R.string.something_wrong), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCbTerms.setChecked(false);
            }
        });
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        hideLoadingDialog();
        switch (requestCode) {
            case WsUtils.WS_CODE_ADD_PROFILE:
                responseForAddProfile(response);
                break;
            case WsUtils.WS_CODE_EDIT_PROFILE:
                responseForEditProfile(response);
                break;
            default:
                break;
        }
    }

    private void responseForAddProfile(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    JSONObject mDataObject = mDataArray.getJSONObject(0);

                    UserDetails.getInstance(this).setUserName(mDataObject.optString("username"));
                    UserDetails.getInstance(this).setUserProfileImage(mDataObject.optString("profile"));
                    UserDetails.getInstance(this).setHowILookActivity(true);

                    navigateActivity(new Intent(this, HomeActivity.class), true);
                } else {
                    PopUtils.alertDialog(this, message, null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void responseForEditProfile(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    JSONObject mDataObject = mDataArray.getJSONObject(0);
                    UserDetails.getInstance(this).setUserName(mDataObject.optString("username"));
                    UserDetails.getInstance(this).setUserProfileImage(mDataObject.optString("profile"));

                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                    onBackPressed();
                } else {
                    PopUtils.alertDialog(this, message, null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        setBackClicked();
    }

    private void setBackClicked() {
        if (fromscreen.equalsIgnoreCase("LOGIN")) {
            if (backClick >= 1) {
                finish();
                System.exit(0);
            } else {
                backClick = backClick + 1;
                Toast.makeText(this, getString(R.string.please_click_again_for_exit), Toast.LENGTH_SHORT).show();
            }
        } else if (fromscreen.equalsIgnoreCase("SETTINGS")) {
            super.onBackPressed();
        }
    }

    @Override
    public void cropImageInterface(Bitmap bitmap, String bitmapString) {
        if (bitmap != null) {
            mImgUser.setImageBitmap(bitmap);
            profileImageString = Base64.encodeToString(PopUtils.getBytesFromBitmap(bitmap), Base64.NO_WRAP);
        }
    }
}

