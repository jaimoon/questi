package com.versatilemobitech.questi.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.adapters.HomeAdapter;
import com.versatilemobitech.questi.adapters.LikedAnswersAdapter;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.models.DialogList;
import com.versatilemobitech.questi.models.LikedAnswers;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.StaticUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Excentd12 on 7/27/2017.
 */

public class LikedAnswersActivity extends BaseActivity implements View.OnClickListener, IParseListener {
    private TextView mTxtContent, mTxtTitle;
    private ImageView mImgBack;
    private RecyclerView mRecyclerView;
    private ArrayList<LikedAnswers> mLikedAnswers = new ArrayList<>();
    private ArrayList<DialogList> mDialogList = new ArrayList<>();
    private int mPageCount = 1;
    private LinearLayoutManager mLayoutManager;
    private boolean canScroll = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liked_answers);
        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();

        if (PopUtils.checkInternetConnection(this)) {
            requestForLikedAnswersWS(true);
        } else {
            PopUtils.alertDialog(this, getString(R.string.pls_check_internet), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                    if (canScroll) {
                        mPageCount = mPageCount + 1;
                        requestForLikedAnswersWS(true);
                    }
                }
            }
        });
    }

    private void setReferences() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mTxtTitle = (TextView) findViewById(R.id.txtTitle);
        mTxtTitle.setText(getString(R.string.liked_answers));

        mTxtContent = (TextView) findViewById(R.id.txtContent);
        mImgBack = (ImageView) findViewById(R.id.imgBack);
    }

    private void setClickListeners() {
        mImgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    private void requestForLikedAnswersWS(boolean showLoading) {
        if (showLoading) {
            showLoadingDialog("Loading", false);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(this).getUserId());
        params.put("page", mPageCount + "");
        params.put("no_of_records", StaticUtils.PAGERECORDS + "");
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_POST_LIKED_ANSWER, params, this, WsUtils.WS_CODE_LIKED_ANSWERS);
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        hideLoadingDialog();
        if (mPageCount == 1) {
            PopUtils.alertDialog(this, getString(R.string.something_wrong), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
        }
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        hideLoadingDialog();
        switch (requestCode) {
            case WsUtils.WS_CODE_LIKED_ANSWERS:
                responseForLikedAnswers(response);
                break;
            default:
                break;
        }
    }

    private void responseForLikedAnswers(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    if (mPageCount == 1) {
                        mLikedAnswers.clear();
                    }
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mTxtContent.setVisibility(View.GONE);

                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    for (int i = 0; i < mDataArray.length(); i++) {
                        JSONObject mDataObject = mDataArray.getJSONObject(i);

                        mLikedAnswers.add(new LikedAnswers(mDataObject.optInt("Question_id"), mDataObject.optInt("spam id"),
                                mDataObject.optInt("Total Likes"), mDataObject.optInt("Your like"), mDataObject.optString("Profile Pic"),
                                mDataObject.optString("Username"), mDataObject.optString("Question"), mDataObject.optString("Answer text"),
                                mDataObject.optString("Answer image"), mDataObject.optString("Answer video"), mDataObject.optString("Posted date")));
                    }

                    setAdapter();
                } else {
                    if (mPageCount == 1) {
                        mRecyclerView.setVisibility(View.GONE);
                        mTxtContent.setVisibility(View.VISIBLE);
                    } else {
                        canScroll = false;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setAdapter() {
        mLayoutManager = new LinearLayoutManager(this);
        LikedAnswersAdapter mLikedAnswersAdapter = new LikedAnswersAdapter(this, mLikedAnswers, new LikedAnswersActivity(),
                mRecyclerView, mTxtContent);
        mRecyclerView.setAdapter(mLikedAnswersAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnItemTouchListener(new HomeAdapter(this, new HomeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        }));
    }
}
