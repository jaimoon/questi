package com.versatilemobitech.questi.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.utils.NetworkChecking;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Login2Activity extends BaseActivity implements View.OnClickListener, IParseListener {

    private TextView welcome_txt, please_txt, you_txt, mTxtLoginFb;
    TextInputLayout code_til,mobile_til;
    private EditText code_edt,mobile_edt;
    Button otp_btn;
    private CallbackManager callbackManager;
    private String facebookId, facebookName, facebookEmail;

    Typeface regular, bold;
    private boolean checkInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        PopUtils.SetLocalization(this, UserDetails.getInstance(this).getLanguage());
        UserDetails.getInstance(this).setChooseLanguageActivity(true);

        //initComponents();

        checkInternet = NetworkChecking.isConnected(this);

        regular = Typeface.createFromAsset(getAssets(), "Montserrat-Regular.ttf");
        bold = Typeface.createFromAsset(getAssets(), "Montserrat-Bold.ttf");

        welcome_txt = findViewById(R.id.welcome_txt);
        welcome_txt.setTypeface(bold);
        please_txt = findViewById(R.id.please_txt);
        please_txt.setTypeface(regular);
        you_txt = findViewById(R.id.you_txt);
        you_txt.setTypeface(regular);

        code_til = findViewById(R.id.code_til);
        code_edt = findViewById(R.id.code_edt);
        code_edt.setTypeface(regular);
        mobile_til = findViewById(R.id.mobile_til);
        mobile_edt = findViewById(R.id.mobile_edt);
        mobile_edt.setTypeface(regular);

        otp_btn = findViewById(R.id.otp_btn);
        otp_btn.setTypeface(bold);
        otp_btn.setOnClickListener(this);

        mTxtLoginFb = findViewById(R.id.txtLoginFb);
        mTxtLoginFb.setTypeface(bold);
        mTxtLoginFb.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v == otp_btn) {

            if (checkInternet) {

                StringRequest stringRequest = new StringRequest(Request.Method.POST, WebServices.LOGIN + "1",
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {

                                Log.d("RESP", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    Log.d("VVVVVV", response);
                                    String status = jsonObject.getString("status");
                                    String message = jsonObject.getString("message");

                                    if (status.equals("200")) {

                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                        String userid = jsonObject1.optString("userid");
                                        String country_code = jsonObject1.optString("country_code");
                                        String mobile = jsonObject1.optString("mobile");
                                        String following = jsonObject1.optString("following");
                                        String follower = jsonObject1.optString("follower");

                                        Intent intent = new Intent(Login2Activity.this, OtpActivity.class);
                                        intent.putExtra("mobile",mobile_edt.getText().toString());
                                        startActivity(intent);

                                    } else {
                                        Toast.makeText(Login2Activity.this, "Invalid Details..!", Toast.LENGTH_SHORT).show();
                                    }

                                    String loginStatus = jsonObject.optString("loginStatus");
                                    String count = jsonObject.optString("count");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {


                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();

                        params.put("country_code", code_edt.getText().toString().trim());
                        params.put("mobile", mobile_edt.getText().toString().trim());
                        Log.d("PARAMS", code_edt.getText().toString().trim() + "\n" + mobile_edt.getText().toString().trim());
                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(Login2Activity.this);
                requestQueue.add(stringRequest);

            }
        }else {

            Toast.makeText(this, "Check Internet Connection..!", Toast.LENGTH_SHORT).show();
            
        }

        if (v == mTxtLoginFb){
            FacebookLoginMethod();
        }

    }

    private void FacebookLoginMethod() {
//        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("the facebook results", object.toString());
                        if (response.getError() != null) {
                            Log.d("", "Error in Response " + response);
                        } else {
                            facebookId = object.optString("id") + "";
                            facebookEmail = object.optString("email") + "";
                            facebookName = object.optString("name") + "";
                            Log.d("the id is: ", facebookId + ", the email id is: " + facebookEmail + ", the name is: " + facebookName);

                            if (PopUtils.checkInternetConnection(Login2Activity.this)) {
                                requestForFacebookLoginWS();
                            } else {
                                PopUtils.alertDialog(Login2Activity.this, getString(R.string.pls_check_internet), null);
                            }
                        }
                    }
                });

                Bundle bundle = new Bundle();
                bundle.putString("fields", "id,email,name");
                graphRequest.setParameters(bundle);
                graphRequest.executeAsync();


//                GraphRequestAsyncTask mGraphRequestAsyncTask = new GraphRequest(loginResult.getAccessToken(),
//                        "/me/friends", null, HttpMethod.GET, new GraphRequest.Callback() {
//                    @Override
//                    public void onCompleted(GraphResponse response) {
//                        try {
//                            JSONObject mJsonObject = new JSONObject(String.valueOf(response));
//                            Log.d("the response friends is", response.toString());
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }).executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(Login2Activity.this, "Facebook login is cancelled, please try again.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                if (exception instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();

                        FacebookLoginMethod();
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /*private void requestForLoginWS() {
        showLoadingDialog(getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("email", code_edt.getText().toString());
        params.put("from", "register");
        params.put("password", mEdtPassword.getText().toString());
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_LOGIN, params, this, WsUtils.WS_CODE_LOGIN);
    }*/

    private void requestForFacebookLoginWS() {
        showLoadingDialog(getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("email", facebookEmail);
        params.put("from", "facebook");
        params.put("facebook_id", facebookId);
        params.put("latitude", "132564");
        params.put("longitude", "147853");
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_LOGIN, params, this, WsUtils.WS_CODE_FACEBOOK_LOGIN);
    }

    private void requestForForgotPasswordWS(String emailId) {
        showLoadingDialog(getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("email", emailId);
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_FORGOT_PASSWORD, params, this, WsUtils.WS_CODE_FORGOT_PASSWORD);
    }

    View.OnClickListener submitClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (PopUtils.checkInternetConnection(Login2Activity.this)) {
                requestForForgotPasswordWS(view.getTag() + "");
            } else {
                PopUtils.alertDialog(Login2Activity.this, getString(R.string.pls_check_internet), null);
            }
        }
    };

    /*public String checkValidation() {
        String message = "";
        if (mEdtEmailId.getText().toString().trim().length() == 0) {
            message = getString(R.string.please_enter_email_or_username);
        } else if (mEdtPassword.getText().toString().trim().length() == 0) {
            message = getString(R.string.please_enter_password);
        } else if (mEdtPassword.getText().toString().trim().length() < 6) {
            message = getString(R.string.entered_password_should_be_minimum_six_letters);
        }
        return message;
    }*/

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        hideLoadingDialog();
        PopUtils.alertDialog(this, getString(R.string.something_wrong), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        hideLoadingDialog();
        switch (requestCode) {
            case WsUtils.WS_CODE_LOGIN:
                responseForLogin(response);
                break;
            case WsUtils.WS_CODE_FORGOT_PASSWORD:
                responseForForgotPassword(response);
                break;
            case WsUtils.WS_CODE_FACEBOOK_LOGIN:
                responseForFacebookLogin(response);
                break;
            default:
                break;
        }
    }

    private void responseForForgotPassword(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    PopUtils.alertDialog(this, message, null);
                } else {
                    PopUtils.alertDialog(this, message, null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void responseForLogin(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    JSONObject mDataObject = mDataArray.getJSONObject(0);

                    UserDetails.getInstance(this).setUserId(mDataObject.optString("Userid"));
                    UserDetails.getInstance(this).setUserEmail(mDataObject.optString("email"));
                    UserDetails.getInstance(this).setUserName(mDataObject.optString("username"));
                    UserDetails.getInstance(this).setUserProfileImage(mDataObject.optString("profile"));
                    UserDetails.getInstance(this).setUserFollowing(mDataObject.optString("following"));
                    UserDetails.getInstance(this).setUserFollowers(mDataObject.optString("follower"));

                    if (!UserDetails.getInstance(this).getUserProfileImage().equalsIgnoreCase("")) {
                        navigateActivity(new Intent(this, HomeActivity.class), true);
                    } else {
                        Intent intent = new Intent(this, HowYouLookActivity.class);
                        intent.putExtra("FROM", "LOGIN");
                        navigateActivity(intent, true);
                    }
                } else {
                    PopUtils.alertDialog(this, message, null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void responseForFacebookLogin(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    JSONObject mDataObject = mDataArray.getJSONObject(0);

                    UserDetails.getInstance(this).setUserId(mDataObject.optString("userid"));
                    UserDetails.getInstance(this).setUserEmail(mDataObject.optString("Email"));
                    UserDetails.getInstance(this).setUserName(mDataObject.optString("username"));
                    UserDetails.getInstance(this).setUserProfileImage(mDataObject.optString("profile"));
                    UserDetails.getInstance(this).setUserFollowing(mDataObject.optString("following"));
                    UserDetails.getInstance(this).setUserFollowers(mDataObject.optString("follower"));

                    if (!UserDetails.getInstance(this).getUserProfileImage().equalsIgnoreCase("")) {
                        navigateActivity(new Intent(this, HomeActivity.class), true);
                    } else {
                        Intent intent = new Intent(this, HowYouLookActivity.class);
                        intent.putExtra("FROM", "LOGIN");
                        navigateActivity(intent, true);
                    }
                } else {
                    PopUtils.alertDialog(this, message, null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Login2Activity.this.finishAffinity();
    }
}