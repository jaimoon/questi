package com.versatilemobitech.questi.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

public class LoginActivity extends BaseActivity implements View.OnClickListener, IParseListener {
    private TextView mTxtForgotPwd, mTxtLogin, mTxtSignUp, mTxtLoginFb;
    private EditText mEdtEmailId, mEdtPassword;
    private CallbackManager callbackManager;
    private String facebookId, facebookName, facebookEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        PopUtils.SetLocalization(this, UserDetails.getInstance(this).getLanguage());
        UserDetails.getInstance(this).setChooseLanguageActivity(true);

        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
    }

    private void setReferences() {
        mTxtForgotPwd = (TextView) findViewById(R.id.txtForgot);
        mTxtLogin = (TextView) findViewById(R.id.txtLogin);
        mTxtSignUp = (TextView) findViewById(R.id.txtSignUp);
        mTxtLoginFb = (TextView) findViewById(R.id.txtLoginFb);
        mEdtEmailId = (EditText) findViewById(R.id.edtEmailId);
        mEdtPassword = (EditText) findViewById(R.id.edtPassword);

//        mEdtEmailId.setText("satishk.versatilemobitech@gmail.com");
//        mEdtPassword.setText("1234567");
    }

    private void setClickListeners() {
        mTxtForgotPwd.setOnClickListener(this);
        mTxtLogin.setOnClickListener(this);
        mTxtSignUp.setOnClickListener(this);
        mTxtLoginFb.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtForgot:
                PopUtils.oneEditBoxDialog(this, getString(R.string.forgot_password), getString(R.string.email_address), submitClick);
                break;
            case R.id.txtLogin:
                if (!checkValidation().equalsIgnoreCase("")) {
                    PopUtils.alertDialog(this, checkValidation(), null);
                } else {
                    if (PopUtils.checkInternetConnection(this)) {
                        requestForLoginWS();
                    } else {
                        PopUtils.alertDialog(this, getString(R.string.pls_check_internet), null);
                    }
                }
                break;
            case R.id.txtSignUp:
                navigateActivity(new Intent(this, RegistrationActivity.class), false);
                break;
            case R.id.txtLoginFb:
                FacebookLoginMethod();
                break;
            default:
                break;
        }
    }

    private void FacebookLoginMethod() {
//        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("the facebook results", object.toString());
                        if (response.getError() != null) {
                            Log.d("", "Error in Response " + response);
                        } else {
                            facebookId = object.optString("id") + "";
                            facebookEmail = object.optString("email") + "";
                            facebookName = object.optString("name") + "";
                            Log.d("the id is: ", facebookId + ", the email id is: " + facebookEmail + ", the name is: " + facebookName);

                            if (PopUtils.checkInternetConnection(LoginActivity.this)) {
                                requestForFacebookLoginWS();
                            } else {
                                PopUtils.alertDialog(LoginActivity.this, getString(R.string.pls_check_internet), null);
                            }
                        }
                    }
                });

                Bundle bundle = new Bundle();
                bundle.putString("fields", "id,email,name");
                graphRequest.setParameters(bundle);
                graphRequest.executeAsync();


//                GraphRequestAsyncTask mGraphRequestAsyncTask = new GraphRequest(loginResult.getAccessToken(),
//                        "/me/friends", null, HttpMethod.GET, new GraphRequest.Callback() {
//                    @Override
//                    public void onCompleted(GraphResponse response) {
//                        try {
//                            JSONObject mJsonObject = new JSONObject(String.valueOf(response));
//                            Log.d("the response friends is", response.toString());
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }).executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Facebook login is cancelled, please try again.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                if (exception instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();

                        FacebookLoginMethod();
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void requestForLoginWS() {
        showLoadingDialog(getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("email", mEdtEmailId.getText().toString());
        params.put("from", "register");
        params.put("password", mEdtPassword.getText().toString());
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_LOGIN, params, this, WsUtils.WS_CODE_LOGIN);
    }

    private void requestForFacebookLoginWS() {
        showLoadingDialog(getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("email", facebookEmail);
        params.put("from", "facebook");
        params.put("facebook_id", facebookId);
        params.put("latitude", "132564");
        params.put("longitude", "147853");
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_LOGIN, params, this, WsUtils.WS_CODE_FACEBOOK_LOGIN);
    }

    private void requestForForgotPasswordWS(String emailId) {
        showLoadingDialog(getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("email", emailId);
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_FORGOT_PASSWORD, params, this, WsUtils.WS_CODE_FORGOT_PASSWORD);
    }

    View.OnClickListener submitClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (PopUtils.checkInternetConnection(LoginActivity.this)) {
                requestForForgotPasswordWS(view.getTag() + "");
            } else {
                PopUtils.alertDialog(LoginActivity.this, getString(R.string.pls_check_internet), null);
            }
        }
    };

    public String checkValidation() {
        String message = "";
        if (mEdtEmailId.getText().toString().trim().length() == 0) {
            message = getString(R.string.please_enter_email_or_username);
        } else if (mEdtPassword.getText().toString().trim().length() == 0) {
            message = getString(R.string.please_enter_password);
        } else if (mEdtPassword.getText().toString().trim().length() < 6) {
            message = getString(R.string.entered_password_should_be_minimum_six_letters);
        }
        return message;
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        hideLoadingDialog();
        PopUtils.alertDialog(this, getString(R.string.something_wrong), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        hideLoadingDialog();
        switch (requestCode) {
            case WsUtils.WS_CODE_LOGIN:
                responseForLogin(response);
                break;
            case WsUtils.WS_CODE_FORGOT_PASSWORD:
                responseForForgotPassword(response);
                break;
            case WsUtils.WS_CODE_FACEBOOK_LOGIN:
                responseForFacebookLogin(response);
                break;
            default:
                break;
        }
    }

    private void responseForForgotPassword(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    PopUtils.alertDialog(this, message, null);
                } else {
                    PopUtils.alertDialog(this, message, null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void responseForLogin(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    JSONObject mDataObject = mDataArray.getJSONObject(0);

                    UserDetails.getInstance(this).setUserId(mDataObject.optString("Userid"));
                    UserDetails.getInstance(this).setUserEmail(mDataObject.optString("email"));
                    UserDetails.getInstance(this).setUserName(mDataObject.optString("username"));
                    UserDetails.getInstance(this).setUserProfileImage(mDataObject.optString("profile"));
                    UserDetails.getInstance(this).setUserFollowing(mDataObject.optString("following"));
                    UserDetails.getInstance(this).setUserFollowers(mDataObject.optString("follower"));

                    if (!UserDetails.getInstance(this).getUserProfileImage().equalsIgnoreCase("")) {
                        navigateActivity(new Intent(this, HomeActivity.class), true);
                    } else {
                        Intent intent = new Intent(this, HowYouLookActivity.class);
                        intent.putExtra("FROM", "LOGIN");
                        navigateActivity(intent, true);
                    }
                } else {
                    PopUtils.alertDialog(this, message, null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void responseForFacebookLogin(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    JSONObject mDataObject = mDataArray.getJSONObject(0);

                    UserDetails.getInstance(this).setUserId(mDataObject.optString("userid"));
                    UserDetails.getInstance(this).setUserEmail(mDataObject.optString("Email"));
                    UserDetails.getInstance(this).setUserName(mDataObject.optString("username"));
                    UserDetails.getInstance(this).setUserProfileImage(mDataObject.optString("profile"));
                    UserDetails.getInstance(this).setUserFollowing(mDataObject.optString("following"));
                    UserDetails.getInstance(this).setUserFollowers(mDataObject.optString("follower"));

                    if (!UserDetails.getInstance(this).getUserProfileImage().equalsIgnoreCase("")) {
                        navigateActivity(new Intent(this, HomeActivity.class), true);
                    } else {
                        Intent intent = new Intent(this, HowYouLookActivity.class);
                        intent.putExtra("FROM", "LOGIN");
                        navigateActivity(intent, true);
                    }
                } else {
                    PopUtils.alertDialog(this, message, null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        LoginActivity.this.finishAffinity();
    }
}

