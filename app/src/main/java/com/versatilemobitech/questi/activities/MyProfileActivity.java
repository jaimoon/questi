package com.versatilemobitech.questi.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.adapters.UserQuestionsAdapter;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.models.UserQuestions;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Excentd11 on 8/21/2017.
 */

public class MyProfileActivity extends BaseActivity implements View.OnClickListener, IParseListener {
    private ImageView mImgUser, mImgBack, mImgMore, mImgAnswer, mImgAnswerUser;
    private TextView mTxtUserName, mTxtFollowers, mTxtFollowing, mTxtTitle, mTxtContent, mTxtQuestion, mTxtAnswer, mTxtLikes;
    private RecyclerView mRecyclerView;
    private LinearLayout mLlQuestion, mLlFollowers, mLlFollowings;
    private String userId = "", profilePic = "";
    private int followStatus = 0, followers, followings;
    private boolean refreshScreen = false;
    private ArrayList<UserQuestions> userQuestionsList = new ArrayList<>();

    private UserQuestionsAdapter mUserQuestionsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();

        if (PopUtils.checkInternetConnection(this)) {
            requestForMyprofileWS();
        } else {
            PopUtils.alertDialog(this, getString(R.string.pls_check_internet), null);
        }
    }

    private void setReferences() {
        mTxtTitle = (TextView) findViewById(R.id.txtTitle);
        mTxtTitle.setText(getString(R.string.my_profile));

        mTxtUserName = (TextView) findViewById(R.id.txtUserName);
        mTxtFollowers = (TextView) findViewById(R.id.txtFollowers);
        mTxtFollowing = (TextView) findViewById(R.id.txtFollowing);
        mTxtContent = (TextView) findViewById(R.id.txtContent);

        mTxtQuestion = (TextView) findViewById(R.id.txtQuestion);
        mTxtAnswer = (TextView) findViewById(R.id.txtAnswer);
        mTxtLikes = (TextView) findViewById(R.id.txtLikes);
        mImgMore = (ImageView) findViewById(R.id.imgMore);
        mImgAnswer = (ImageView) findViewById(R.id.imgAnswer);
        mImgAnswerUser = (ImageView) findViewById(R.id.imgAnswerUser);

        mImgUser = (ImageView) findViewById(R.id.imgUser);
        mImgBack = (ImageView) findViewById(R.id.imgBack);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mLlQuestion = (LinearLayout) findViewById(R.id.llQuestion);
        mLlFollowers = (LinearLayout) findViewById(R.id.llFollowers);
        mLlFollowings = (LinearLayout) findViewById(R.id.llFollowings);
    }

    private void setClickListeners() {
        mImgUser.setOnClickListener(this);
        mImgBack.setOnClickListener(this);
        mImgMore.setOnClickListener(this);
        mTxtFollowers.setOnClickListener(this);
        mTxtFollowing.setOnClickListener(this);
        mLlFollowers.setOnClickListener(this);
        mLlFollowings.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                MyProfileActivity.this.finish();
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                break;
            case R.id.imgMore:
                Intent mIntent = new Intent(this, UserQuestionsActivity.class);
                mIntent.putExtra("USERNAME", mTxtUserName.getText().toString());
                mIntent.putExtra("QUESTIONSLIST", userQuestionsList);
                mIntent.putExtra("PROFILEPIC", profilePic);
                startActivity(mIntent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                break;
            case R.id.llFollowers:
                Intent mIntent1 = new Intent(this, FollowersActivity.class);
                mIntent1.putExtra("USERID", UserDetails.getInstance(this).getUserId());
                startActivity(mIntent1);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                break;
            case R.id.llFollowings:
                Intent mIntent2 = new Intent(this, FollowingsActivity.class);
                mIntent2.putExtra("USERID", UserDetails.getInstance(this).getUserId());
                startActivity(mIntent2);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        MyProfileActivity.this.finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    private void requestForMyprofileWS() {
        showLoadingDialog(getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(this).getUserId());
        params.put("profileid", UserDetails.getInstance(this).getUserId());
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_MY_PROFILE, params, this, WsUtils.WS_CODE_MY_PROFILE);
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        hideLoadingDialog();
        PopUtils.alertDialog(this, getString(R.string.something_wrong), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        hideLoadingDialog();
        switch (requestCode) {
            case WsUtils.WS_CODE_MY_PROFILE:
                responseForMyProfile(response);
                break;
            default:
                break;
        }
    }

    private void responseForMyProfile(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    JSONObject mDataObject = mDataArray.getJSONObject(0);

                    mTxtUserName.setText(mDataObject.optString("Username"));
                    mTxtFollowers.setText(mDataObject.optString("follower"));
                    mTxtFollowing.setText(mDataObject.optString("following"));
                    Picasso.with(this).load(mDataObject.optString("profile")).into(mImgUser);
                    profilePic = mDataObject.optString("profile");

                    JSONObject mQuestionsObject = mDataObject.getJSONObject("Questions");
                    if (mQuestionsObject.optString("status").equalsIgnoreCase("200")) {
                        mTxtContent.setVisibility(View.GONE);
                        mLlQuestion.setVisibility(View.GONE);

                        JSONArray mQuestionsDataArray = mQuestionsObject.getJSONArray("data");
                        for (int i = 0; i < mQuestionsDataArray.length(); i++) {
                            JSONObject mQuestionsDataObject = mQuestionsDataArray.getJSONObject(i);
                            userQuestionsList.add(new UserQuestions(mQuestionsDataObject.optInt("question_id"),
                                    mQuestionsDataObject.optInt("likes"), mQuestionsDataObject.optString("question"),
                                    mQuestionsDataObject.optString("answer_text"), mQuestionsDataObject.optString("answer_image"),
                                    mQuestionsDataObject.optString("answer_video")));

                            /*setting first question in the list to the screen*/
                            mTxtQuestion.setText(userQuestionsList.get(0).question);
                            Picasso.with(this).load(mDataObject.optString("profile")).error(R.drawable.profile_placeholder)
                                    .placeholder(R.drawable.profile_placeholder).into(mImgAnswerUser);
                            if (!userQuestionsList.get(0).answerText.equalsIgnoreCase("")) {
                                mTxtAnswer.setVisibility(View.VISIBLE);
                                mTxtAnswer.setText(userQuestionsList.get(0).answerText);
                            } else {
                                mTxtAnswer.setVisibility(View.GONE);
                            }
                            if (!userQuestionsList.get(0).answerImage.equalsIgnoreCase(" ")) {
                                mImgAnswer.setVisibility(View.VISIBLE);
                                Picasso.with(this).load(userQuestionsList.get(0).answerImage).error(R.drawable.answer_placeholder).
                                        placeholder(R.drawable.answer_placeholder).into(mImgAnswer);
                            } else {
                                mImgAnswer.setVisibility(View.GONE);
                            }
                            mTxtLikes.setText(userQuestionsList.get(0).likes + "");

                        }

                        setAdapter();

                    } else {
                        mTxtContent.setVisibility(View.VISIBLE);
                        mLlQuestion.setVisibility(View.GONE);
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mUserQuestionsAdapter = new UserQuestionsAdapter(this, userQuestionsList, profilePic);
        mRecyclerView.setAdapter(mUserQuestionsAdapter);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addOnItemTouchListener(new UserQuestionsAdapter(this, new UserQuestionsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }
        }));
    }
}
