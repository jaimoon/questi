package com.versatilemobitech.questi.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.utils.NetworkChecking;

import in.aabhasjindal.otptextview.OtpTextView;

public class OtpActivity extends AppCompatActivity implements View.OnClickListener {

    Typeface regular, bold;
    private boolean checkInternet;
    TextView otp_txt,pleas_txt,mobile_txt,dont_txt,resend_txt;
    Button verify_btn;
    OtpTextView otp_view;
    String mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        checkInternet = NetworkChecking.isConnected(this);

        regular = Typeface.createFromAsset(getAssets(), "Montserrat-Regular.ttf");
        bold = Typeface.createFromAsset(getAssets(), "Montserrat-Bold.ttf");

        mobile = getIntent().getStringExtra("mobile");

        otp_txt = findViewById(R.id.otp_txt);
        otp_txt.setTypeface(bold);
        pleas_txt = findViewById(R.id.pleas_txt);
        pleas_txt.setTypeface(regular);
        mobile_txt = findViewById(R.id.mobile_txt);
        mobile_txt.setTypeface(regular);
        mobile_txt.setText(mobile);
        otp_view = findViewById(R.id.otp_view);
        dont_txt = findViewById(R.id.dont_txt);
        dont_txt.setTypeface(regular);
        resend_txt = findViewById(R.id.resend_txt);
        resend_txt.setTypeface(bold);
        resend_txt.setOnClickListener(this);
        verify_btn = findViewById(R.id.verify_btn);
        verify_btn.setTypeface(regular);
        verify_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == resend_txt){

            if (checkInternet){

            }else {
                Toast.makeText(this, "Check Internet Connection..!", Toast.LENGTH_SHORT).show();
            }

        }

        if (v == verify_btn){

            if (checkInternet){

                Intent intent = new Intent(OtpActivity.this, HomeActivity.class);
                startActivity(intent);

            }else {
                Toast.makeText(this, "Check Internet Connection..!", Toast.LENGTH_SHORT).show();
            }

        }
    }
}