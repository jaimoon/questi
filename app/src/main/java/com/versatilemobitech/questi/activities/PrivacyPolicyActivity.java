package com.versatilemobitech.questi.activities;

/**
 * Created by Excentd12 on 7/27/2017.
 */

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.questi.R;

public class PrivacyPolicyActivity extends BaseActivity implements View.OnClickListener {
    private TextView mTxtTitle, mTxtContent;
    private ImageView mImgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
    }


    private void setReferences() {
        mTxtTitle = (TextView) findViewById(R.id.txtTitle);
        mTxtTitle.setText(getString(R.string.privacy_policy));
        mTxtContent = (TextView) findViewById(R.id.txtContent);
        mTxtContent.setText(getString(R.string.privacy_policy_text));
        mImgBack = (ImageView) findViewById(R.id.imgBack);
    }

    private void setClickListeners() {
        mImgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            default:
                break;
        }
    }
}
