package com.versatilemobitech.questi.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.adapters.UserQuestionsAdapter;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.models.UserQuestions;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Excentd12 on 7/27/2017.
 */

public class ProfileActivity extends BaseActivity implements View.OnClickListener, IParseListener {
    private ImageView mImgUser, mImgBack, mImgMore, mImgAnswer, mImgAnswerUser;
    private TextView mTxtFollow, mTxtUserName, mTxtFollowers, mTxtFollowers1, mTxtFollowing, mTxtFollowing1, mTxtTitle, mTxtContent, mTxtQuestion, mTxtAnswer,
            mTxtLikes, mTxtAskQuestion;
    private RecyclerView mRecyclerView;
    private LinearLayout mLlQuestion;
    private String userId = "", profilePic = "", from = "";
    private int followStatus = 0, followers, followings;
    private boolean refreshScreen = false;
    private ArrayList<UserQuestions> userQuestionsList = new ArrayList<>();
    private UserQuestionsAdapter mUserQuestionsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
        getIntentData();

        if (PopUtils.checkInternetConnection(this)) {
            requestForprofileWS();
        } else {
            PopUtils.alertDialog(this, getString(R.string.pls_check_internet), null);
        }
    }

    private void setReferences() {
        mTxtTitle = (TextView) findViewById(R.id.txtTitle);
        mTxtTitle.setText(getString(R.string.profile));

        mTxtFollow = (TextView) findViewById(R.id.txtFollow);
        mTxtUserName = (TextView) findViewById(R.id.txtUserName);
        mTxtFollowers = (TextView) findViewById(R.id.txtFollowers);
        mTxtFollowers1 = (TextView) findViewById(R.id.txtFollowers1);
        mTxtFollowing = (TextView) findViewById(R.id.txtFollowing);
        mTxtFollowing1 = (TextView) findViewById(R.id.txtFollowing1);
        mTxtContent = (TextView) findViewById(R.id.txtContent);
        mTxtAskQuestion = (TextView) findViewById(R.id.txtAskQuestion);

        mTxtQuestion = (TextView) findViewById(R.id.txtQuestion);
        mTxtAnswer = (TextView) findViewById(R.id.txtAnswer);
        mTxtLikes = (TextView) findViewById(R.id.txtLikes);
        mImgMore = (ImageView) findViewById(R.id.imgMore);
        mImgAnswer = (ImageView) findViewById(R.id.imgAnswer);
        mImgAnswerUser = (ImageView) findViewById(R.id.imgAnswerUser);

        mImgUser = (ImageView) findViewById(R.id.imgUser);
        mImgBack = (ImageView) findViewById(R.id.imgBack);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mLlQuestion = (LinearLayout) findViewById(R.id.llQuestion);
    }

    private void setClickListeners() {
        mTxtFollow.setOnClickListener(this);
        mTxtFollowers.setOnClickListener(this);
        mTxtFollowers1.setOnClickListener(this);
        mTxtFollowing.setOnClickListener(this);
        mTxtFollowing1.setOnClickListener(this);
        mImgUser.setOnClickListener(this);
        mImgBack.setOnClickListener(this);
        mImgMore.setOnClickListener(this);
        mTxtAskQuestion.setOnClickListener(this);
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            userId = bundle.getString("USERID");
            from = bundle.getString("FROM");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtFollow:
                if (PopUtils.checkInternetConnection(this)) {
                    requestForFollowWS();
                } else {
                    PopUtils.alertDialog(this, getString(R.string.pls_check_internet), null);
                }
                break;
            case R.id.txtAskQuestion:
                if (followStatus == 1 || followStatus == 0) {
                    Intent mIntent1 = new Intent(this, AskQuestionActivity.class);
                    mIntent1.putExtra("USERID", userId);
                    mIntent1.putExtra("USERNAME", mTxtUserName.getText().toString());
                    mIntent1.putExtra("PROFILEPIC", profilePic);
                    startActivity(mIntent1);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                } else {
                    PopUtils.alertDialog(this, getString(R.string.please_follow_above_user_before_asking_any_question), null);
                }
                break;
            case R.id.imgBack:
                finish();
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                break;
            case R.id.imgMore:
                Intent mIntent = new Intent(this, UserQuestionsActivity.class);
                mIntent.putExtra("USERNAME", mTxtUserName.getText().toString());
                mIntent.putExtra("QUESTIONSLIST", userQuestionsList);
                mIntent.putExtra("PROFILEPIC", profilePic);
                startActivity(mIntent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                break;
            case R.id.txtFollowers:
                if (followStatus == 1 || followStatus == 0) {
                    Intent mIntent1 = new Intent(this, FollowersActivity.class);
                    mIntent1.putExtra("USERID", userId);
                    startActivity(mIntent1);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                } else {
                    PopUtils.alertDialog(this, getString(R.string.u_cant_see_the_followings_of) + " " +
                            mTxtUserName.getText().toString() + " " + getString(R.string.without_following_him), null);
                }
                break;
            case R.id.txtFollowers1:
                if (followStatus == 1 || followStatus == 0) {
                    Intent mIntent1 = new Intent(this, FollowersActivity.class);
                    mIntent1.putExtra("USERID", userId);
                    startActivity(mIntent1);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                } else {
                    PopUtils.alertDialog(this, getString(R.string.u_cant_see_the_followings_of) + " " +
                            mTxtUserName.getText().toString() + " " + getString(R.string.without_following_him), null);
                }
                break;
            case R.id.txtFollowing:
                if (followStatus == 1 || followStatus == 0) {
                    Intent mIntent1 = new Intent(this, FollowingsActivity.class);
                    mIntent1.putExtra("USERID", userId);
                    startActivity(mIntent1);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                } else {
                    PopUtils.alertDialog(this, getString(R.string.u_cant_see_the_followers_of) + " " +
                            mTxtUserName.getText().toString() + " " + getString(R.string.without_following_him), null);
                }
                break;
            case R.id.txtFollowing1:
                if (followStatus == 1 || followStatus == 0) {
                    Intent mIntent1 = new Intent(this, FollowingsActivity.class);
                    mIntent1.putExtra("USERID", userId);
                    startActivity(mIntent1);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                } else {
                    PopUtils.alertDialog(this, getString(R.string.u_cant_see_the_followers_of) + " " +
                            mTxtUserName.getText().toString() + " " + getString(R.string.without_following_him), null);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (refreshScreen)
        {
            Intent i = new Intent("com.versatilemobitech.questi.REFRESH_SCREEN");
            if (from.equalsIgnoreCase("HOME_FRAGMENT"))
            {
                i.putExtra("VALUE", "1");
            } else if (from.equalsIgnoreCase("EXPLORE_FRAGMENT")) {
                i.putExtra("VALUE", "2");
            } else if (from.equalsIgnoreCase("SEARCH_FRAGMENT")) {
                i.putExtra("VALUE", "3");
            }
            sendBroadcast(i);
            ProfileActivity.this.finish();
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        } else {
            ProfileActivity.this.finish();
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        }
    }

    private void requestForFollowWS() {
        showLoadingDialog(getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(this).getUserId());
        params.put("following_id", userId);
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_FOLLOW, params, this, WsUtils.WS_CODE_FOLLOW);
    }

    private void requestForprofileWS() {
        showLoadingDialog(getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("profileid", userId);
        params.put("userid", UserDetails.getInstance(this).getUserId());
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_MY_PROFILE, params, this, WsUtils.WS_CODE_MY_PROFILE);
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        hideLoadingDialog();
        PopUtils.alertDialog(this, getString(R.string.something_wrong), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        hideLoadingDialog();
        switch (requestCode) {
            case WsUtils.WS_CODE_FOLLOW:
                responseForFollow(response);
                break;
            case WsUtils.WS_CODE_MY_PROFILE:
                responseForProfile(response);
                break;
            default:
                break;
        }
    }

    private void responseForFollow(String response) {
        if (response != null) {
            try {
                final JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    refreshScreen = true;
                    PopUtils.alertDialog(this, message, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (mTxtFollow.getText().toString().equalsIgnoreCase("FOLLOW")) {
                                followStatus = 1;
                                mTxtFollow.setText(getString(R.string.unfollow_));
                                followers = mJsonObject.optInt("follower");
                                mTxtFollowers.setText(followers + "");
                            } else {
                                followStatus = 0;
                                mTxtFollow.setText(getString(R.string.follow));
                                followers = mJsonObject.optInt("follower");
                                mTxtFollowers.setText(followers + "");
                            }
                        }
                    });
                } else {
                    PopUtils.alertDialog(this, message, null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void responseForProfile(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    JSONObject mDataObject = mDataArray.getJSONObject(0);

                    mTxtUserName.setText(mDataObject.optString("Username"));
                    mTxtFollowers.setText(mDataObject.optString("follower"));
                    mTxtFollowing.setText(mDataObject.optString("following"));
                    followStatus = mDataObject.optInt("status");
                    if (followStatus == 0) {
                        mTxtFollow.setText("Follow");
                    } else if (followStatus == 1) {
                        mTxtFollow.setText("UnFollow");
                    }

                    profilePic = mDataObject.optString("profile");
                    if (!profilePic.equalsIgnoreCase("")) {
                        Picasso.with(this).load(profilePic).error(R.drawable.profile_placeholder)
                                .placeholder(R.drawable.profile_placeholder).into(mImgUser);
                    } else {
                        Picasso.with(this).load(R.drawable.profile_placeholder).into(mImgUser);
                    }

                    JSONObject mQuestionsObject = mDataObject.getJSONObject("Questions");
                    if (mQuestionsObject.optString("status").equalsIgnoreCase("200")) {
                        mTxtContent.setVisibility(View.GONE);
                        mLlQuestion.setVisibility(View.GONE);

                        JSONArray mQuestionsDataArray = mQuestionsObject.getJSONArray("data");
                        for (int i = 0; i < mQuestionsDataArray.length(); i++) {
                            JSONObject mQuestionsDataObject = mQuestionsDataArray.getJSONObject(i);
                            userQuestionsList.add(new UserQuestions(mQuestionsDataObject.optInt("question_id"),
                                    mQuestionsDataObject.optInt("likes"), mQuestionsDataObject.optString("question"),
                                    mQuestionsDataObject.optString("answer_text"), mQuestionsDataObject.optString("answer_image"),
                                    mQuestionsDataObject.optString("answer_video")));

                            /*setting first question in the list to the screen*/
                            mTxtQuestion.setText(userQuestionsList.get(0).question);
                            mTxtLikes.setText(userQuestionsList.get(0).likes + "");
                            if (!userQuestionsList.get(0).answerText.equalsIgnoreCase("")) {
                                mTxtAnswer.setVisibility(View.VISIBLE);
                                mTxtAnswer.setText(userQuestionsList.get(0).answerText);
                            } else {
                                mTxtAnswer.setVisibility(View.GONE);
                            }
                            if (!profilePic.equalsIgnoreCase("")) {
                                Picasso.with(this).load(profilePic).error(R.drawable.profile_placeholder)
                                        .placeholder(R.drawable.profile_placeholder).into(mImgAnswerUser);
                            } else {
                                Picasso.with(this).load(R.drawable.profile_placeholder).into(mImgAnswerUser);
                            }
                            if (!userQuestionsList.get(0).answerImage.equalsIgnoreCase(" ")) {
                                mImgAnswer.setVisibility(View.VISIBLE);
                                Picasso.with(this).load(userQuestionsList.get(0).answerImage).error(R.drawable.answer_placeholder).error(R.drawable.answer_placeholder).into(mImgAnswer);
                            } else {
                                mImgAnswer.setVisibility(View.GONE);
                            }
                        }
                        setAdapter();


                    } else {
                        mTxtContent.setVisibility(View.VISIBLE);
                        mLlQuestion.setVisibility(View.GONE);
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setAdapter()
    {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mUserQuestionsAdapter = new UserQuestionsAdapter(this, userQuestionsList, profilePic);
        mRecyclerView.setAdapter(mUserQuestionsAdapter);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.addOnItemTouchListener(new UserQuestionsAdapter(this, new UserQuestionsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }
        }));

        mImgUser.setFocusable(true);
        mImgUser.requestFocus();
    }

}
