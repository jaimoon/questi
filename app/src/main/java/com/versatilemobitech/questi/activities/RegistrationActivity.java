package com.versatilemobitech.questi.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationActivity extends BaseActivity implements View.OnClickListener, IParseListener {
    private EditText mEdtEmailId, mEdtPassword, mEdtConfirmPassword;
    private TextView mTxtSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
    }

    private void setReferences() {
        mTxtSubmit = (TextView) findViewById(R.id.txtSubmit);
        mEdtEmailId = (EditText) findViewById(R.id.edtEmailId);
        mEdtPassword = (EditText) findViewById(R.id.edtPassword);
        mEdtConfirmPassword = (EditText) findViewById(R.id.edtConfirmPassword);
    }

    private void setClickListeners() {
        mTxtSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtSubmit:
                if (!checkValidation().equalsIgnoreCase("")) {
                    PopUtils.alertDialog(this, checkValidation(), null);
                } else {
                    if (PopUtils.checkInternetConnection(this)) {
                        requestForRegistrationWS();
                    } else {
                        PopUtils.alertDialog(this, getString(R.string.pls_check_internet), null);
                    }
                }
                break;
            default:
                break;
        }
    }

    private void requestForRegistrationWS() {
        showLoadingDialog(getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("email", mEdtEmailId.getText().toString());
        params.put("from", "register");
        params.put("latitude", "132564");
        params.put("longitude", "147853");
        params.put("password", mEdtConfirmPassword.getText().toString());
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(this, WebServices.URL_REGISTRATION, params, this, WsUtils.WS_CODE_REGISTRATION);
    }

    public String checkValidation() {
        String message = "";
        if (mEdtEmailId.getText().toString().trim().length() == 0) {
            message = getString(R.string.please_enter_email_id);
            mEdtEmailId.requestFocus();
        } else if (!emailValidator(mEdtEmailId.getText().toString().trim())) {
            message = getString(R.string.please_enter_valid_email);
            mEdtEmailId.setText("");
            mEdtEmailId.requestFocus();
        } else if (mEdtPassword.getText().toString().trim().length() == 0) {
            message = getString(R.string.please_enter_password);
            mEdtPassword.requestFocus();
        } else if (mEdtPassword.getText().toString().trim().length() < 6) {
            message = getString(R.string.entered_password_should_be_minimum_six_letters);
            mEdtPassword.requestFocus();
        } else if (mEdtConfirmPassword.getText().toString().trim().length() == 0) {
            message = getString(R.string.please_enter_confirm_password);
            mEdtConfirmPassword.requestFocus();
        } else if (mEdtConfirmPassword.getText().toString().trim().length() < 6) {
            message = getString(R.string.entered_confirm_password_should_be_minimum_six_letters);
            mEdtConfirmPassword.requestFocus();
        } else if (!mEdtPassword.getText().toString().trim().equalsIgnoreCase(mEdtConfirmPassword.getText().toString().trim())) {
            message = getString(R.string.cofirm_password_and_password_should_be_same_in_letters);
            mEdtPassword.requestFocus();
        }
        return message;
    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        hideLoadingDialog();
        PopUtils.alertDialog(this, getString(R.string.something_wrong), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        hideLoadingDialog();
        switch (requestCode) {
            case WsUtils.WS_CODE_REGISTRATION:
                responseForRegistration(response);
                break;
            default:
                break;
        }
    }

    private void responseForRegistration(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    JSONObject mDataObject = mDataArray.getJSONObject(0);

                    UserDetails.getInstance(this).setUserId(mDataObject.getString("Userid"));
                    UserDetails.getInstance(this).setUserEmail(mDataObject.getString("Email"));
                    UserDetails.getInstance(this).setUserName("");
                    UserDetails.getInstance(this).setUserProfileImage("");
                    UserDetails.getInstance(this).setUserFollowing("");
                    UserDetails.getInstance(this).setUserFollowers("");

                    Intent intent = new Intent(this, HowYouLookActivity.class);
                    intent.putExtra("FROM", "LOGIN");
                    navigateActivity(intent, true);
                } else {
                    PopUtils.alertDialog(this, message, null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
