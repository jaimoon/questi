package com.versatilemobitech.questi.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;

import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

//Api Key : AIzaSyDs22qrE_4ufCJar2K7AEL5dkX1aU6Qwgs
//sha1 : 72:13:93:64:5F:11:C8:AB:0B:CB:CA:37:1D:2F:E7:6F:91:23:46:2F
/*facebook credentials*/
//key hash : chOTZF8RyKsLy8o3HS/nb5EjRi8=
//App ID : 1035878229849196

public class SplashActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        PopUtils.SetLocalization(this, UserDetails.getInstance(this).getLanguage());

//        printHashKey();
        initComponents();
    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.versatilemobitech.questi", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    private void initComponents() {
        setSplashScreen();
    }

    private void setSplashScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (UserDetails.getInstance(SplashActivity.this).getChooseLanguageActivity()) {
                    if (UserDetails.getInstance(SplashActivity.this).getUserId().equalsIgnoreCase("")) {
                        navigateActivity(new Intent(SplashActivity.this, Login2Activity.class), true);
                    } else {
                        if (!UserDetails.getInstance(SplashActivity.this).getUserName().equalsIgnoreCase("")) {
                            navigateActivity(new Intent(SplashActivity.this, HomeActivity.class), true);
                        } else {
                            Intent intent = new Intent(SplashActivity.this, HowYouLookActivity.class);
                            intent.putExtra("FROM", "LOGIN");
                            navigateActivity(intent, true);
//                            navigateActivity(new Intent(SplashActivity.this, HowYouLookActivity.class), true);
                        }
                    }
                } else {
                    navigateActivity(new Intent(SplashActivity.this, Login2Activity.class), true);
//                    navigateActivity(new Intent(SplashActivity.this, ChooseLanguageActivity.class), true);
                }
            }
        }, 2000);
    }
}

