package com.versatilemobitech.questi.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.adapters.UserQuestionsAdapter;
import com.versatilemobitech.questi.models.UserQuestions;

import java.util.ArrayList;

/**
 * Created by Excentd11 on 8/16/2017.
 */

public class UserQuestionsActivity extends BaseActivity implements View.OnClickListener {
    private ArrayList<UserQuestions> mUserQuestions = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private ImageView mImgBack;
    private TextView mTxtTitle;
    private String profilePic = "";
    private UserQuestionsAdapter mUserQuestionsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_questions);

        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
        getIntentData();
    }

    private void setReferences() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mImgBack = (ImageView) findViewById(R.id.imgBack);
        mTxtTitle = (TextView) findViewById(R.id.txtTitle);
    }

    private void setClickListeners() {
        mImgBack.setOnClickListener(this);
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mTxtTitle.setText(bundle.getString("USERNAME"));
            profilePic = bundle.getString("PROFILEPIC");
            mUserQuestions = (ArrayList<UserQuestions>) bundle.getSerializable("QUESTIONSLIST");
            setAdapter();
        }
    }

    private void setAdapter()
    {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mUserQuestionsAdapter = new UserQuestionsAdapter(this, mUserQuestions, profilePic);
        mRecyclerView.setAdapter(mUserQuestionsAdapter);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addOnItemTouchListener(new UserQuestionsAdapter(this, new UserQuestionsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }
        }));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            default:
                break;
        }
    }
}
