package com.versatilemobitech.questi.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.versatilemobitech.questi.R;

public class VideoActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener,
        YouTubePlayer.PlayerStateChangeListener, View.OnClickListener {
    private YouTubePlayerView mYouTubePlayerView;
    private TextView mTxtTitle;
    private ImageView mImgBack;
    private int backClick = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        initComponents();
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
    }

    private void setReferences() {
        mYouTubePlayerView = (YouTubePlayerView) findViewById(R.id.youTubePlayerView);
        mYouTubePlayerView.initialize("AIzaSyDs22qrE_4ufCJar2K7AEL5dkX1aU6Qwgs", this);

        mTxtTitle = (TextView) findViewById(R.id.txtTitle);
        mTxtTitle.setText(getString(R.string.video));

        mImgBack = (ImageView) findViewById(R.id.imgBack);
        mImgBack.setVisibility(View.GONE);
    }

    private void setClickListeners() {
        mImgBack.setOnClickListener(this);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean isCompleted) {
        youTubePlayer.setPlayerStateChangeListener(this);

        if (!isCompleted) {
            youTubePlayer.loadVideo("i84QMAIiLwI");
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, youTubeInitializationResult.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoading() {

    }

    @Override
    public void onLoaded(String s) {

    }

    @Override
    public void onAdStarted() {

    }

    @Override
    public void onVideoStarted() {

    }

    @Override
    public void onVideoEnded() {
        Intent mIntent = new Intent(this, HomeActivity.class);
        startActivity(mIntent);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        finish();
    }

    @Override
    public void onError(YouTubePlayer.ErrorReason errorReason) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        setBackClicked();
    }

    private void setBackClicked() {
        if (backClick >= 1) {
            finish();
            System.exit(0);
        } else {
            Toast.makeText(this, getString(R.string.please_click_again_for_exit), Toast.LENGTH_SHORT).show();
            backClick = backClick + 1;
        }
    }
}
