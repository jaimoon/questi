package com.versatilemobitech.questi.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.models.Followers;

import java.util.ArrayList;

/**
 * Created by Excentd11 on 8/24/2017.
 */

public class FollowersAdapter extends RecyclerView.Adapter<FollowersAdapter.VideoHolder> implements RecyclerView.OnItemTouchListener {
    private Context mContext;
    private FollowersAdapter.OnItemClickListener mListener;
    private GestureDetector mGestureDetector;
    private ArrayList<Followers> mFollowersList;
    private String profilePic = "";

    public FollowersAdapter(Context context, ArrayList<Followers> mFollowersList) {
        mContext = context;
        this.mFollowersList = mFollowersList;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public FollowersAdapter(Context context, FollowersAdapter.OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public FollowersAdapter.VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_followers, parent, false);
        return new FollowersAdapter.VideoHolder(view);
    }

    @Override
    public void onBindViewHolder(final FollowersAdapter.VideoHolder holder, final int position) {
        final Followers mFollowers = mFollowersList.get(position);

        holder.mTxtUserName.setText(mFollowers.userName);
        if (!mFollowers.profilePic.equalsIgnoreCase("")) {
            Picasso.with(mContext).load(mFollowers.profilePic).placeholder(R.drawable.profile_placeholder)
                    .placeholder(R.drawable.profile_placeholder).into(holder.mImgProfilePic);
        } else {
            Picasso.with(mContext).load(R.drawable.profile_placeholder).into(holder.mImgProfilePic);
        }
    }

    @Override
    public int getItemCount() {
        return mFollowersList.size();
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        TextView mTxtUserName;
        ImageView mImgProfilePic;

        public VideoHolder(View view) {
            super(view);
            mTxtUserName = (TextView) view.findViewById(R.id.txtUserName);
            mImgProfilePic = (ImageView) view.findViewById(R.id.imgProfile);
        }
    }
}




