package com.versatilemobitech.questi.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.activities.MyProfileActivity;
import com.versatilemobitech.questi.activities.ProfileActivity;
import com.versatilemobitech.questi.fragments.HomeFragment;
import com.versatilemobitech.questi.interfaces.DialogListInterface;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.models.DialogList;
import com.versatilemobitech.questi.models.HomeModel;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Excentd11 on 8/8/2017.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.VideoHolder> implements RecyclerView.OnItemTouchListener {
    private Context mContext;
    private OnItemClickListener mListener;
    private GestureDetector mGestureDetector;
    private ArrayList<HomeModel> mHomeModelList;
    private HomeFragment mHomeFragment;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ArrayList<DialogList> mDialogList = new ArrayList<>();
    private boolean likeClicked = false;

    public HomeAdapter(Context context, ArrayList<HomeModel> mHomeModelList, HomeFragment homeFragment,
                       SwipeRefreshLayout mSwipeRefreshLayout) {
        mContext = context;
        this.mHomeModelList = mHomeModelList;
        this.mHomeFragment = homeFragment;
        this.mSwipeRefreshLayout = mSwipeRefreshLayout;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public HomeAdapter(Context context, OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public HomeAdapter.VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home, parent, false);
        return new HomeAdapter.VideoHolder(view);
    }

    @Override
    public void onBindViewHolder(final HomeAdapter.VideoHolder holder, final int position) {
        final HomeModel mHomeModel = mHomeModelList.get(position);

        holder.mTxtQuestion.setText(mHomeModel.question);
        holder.mTxtUserName.setText(mHomeModel.userName);
        holder.mTxtTime.setText(mHomeModel.postedDate);

        if (mHomeModel.answerImage.equalsIgnoreCase("")) {
            holder.mImgQuestion.setVisibility(View.GONE);
        } else {
            holder.mImgQuestion.setVisibility(View.VISIBLE);
            Picasso.with(mContext).load(mHomeModel.answerImage).placeholder(R.drawable.answer_placeholder)
                    .error(R.drawable.answer_placeholder).into(holder.mImgQuestion);
        }

        if (mHomeModel.answerText.equalsIgnoreCase("")) {
            holder.mTxtAnswer.setVisibility(View.GONE);
        } else {
            holder.mTxtAnswer.setVisibility(View.VISIBLE);
            holder.mTxtAnswer.setText(mHomeModel.answerText);
        }

        if (mHomeModel.profilePic.equalsIgnoreCase("")) {
            Picasso.with(mContext).load(R.drawable.user_placeholder).placeholder(R.drawable.user_placeholder)
                    .error(R.drawable.user_placeholder).into(holder.mImgUser);
        } else {
            Picasso.with(mContext).load(mHomeModel.profilePic).placeholder(R.drawable.user_placeholder)
                    .error(R.drawable.user_placeholder).into(holder.mImgUser);
        }

        /*For Like functionality*/
        if (mHomeModel.yourLike == 0) {
            holder.mImgLike.setVisibility(View.GONE);
            holder.mImgUnLike.setVisibility(View.VISIBLE);
        } else {
            holder.mImgLike.setVisibility(View.VISIBLE);
            holder.mImgUnLike.setVisibility(View.GONE);
        }

        holder.mImgUnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PopUtils.checkInternetConnection(mContext)) {
                    mHomeFragment.showLoadingDialog(mContext, mContext.getString(R.string.loading), false);
                    HashMap<String, String> params = new HashMap<>();
                    params.put("userid", UserDetails.getInstance(mContext).getUserId());
                    params.put("question_id", mHomeModelList.get(position).questionId + "");
                    ServerResponse serverResponse = new ServerResponse();
                    serverResponse.serviceRequest(mContext, WebServices.URL_LIKE, params, new IParseListener() {
                        @Override
                        public void ErrorResponse(VolleyError error, int requestCode) {
                            mHomeFragment.hideLoadingDialog(mContext);
                        }

                        @Override
                        public void SuccessResponse(String response, int requestCode) {
                            mHomeFragment.hideLoadingDialog(mContext);
                            if (response != null) {
                                try {
                                    JSONObject mJsonObject = new JSONObject(response);
                                    String message = mJsonObject.getString("message");
                                    if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                                        holder.mImgLike.setVisibility(View.VISIBLE);
                                        holder.mImgUnLike.setVisibility(View.GONE);
                                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }, WsUtils.WS_CODE_LIKE);
                } else {
                    PopUtils.alertDialog(mContext, mContext.getString(R.string.pls_check_internet), null);
                }
            }
        });

        holder.mImgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PopUtils.checkInternetConnection(mContext)) {
                    mHomeFragment.showLoadingDialog(mContext, mContext.getString(R.string.loading), false);
                    HashMap<String, String> params = new HashMap<>();
                    params.put("userid", UserDetails.getInstance(mContext).getUserId());
                    params.put("question_id", mHomeModelList.get(position).questionId + "");
                    ServerResponse serverResponse = new ServerResponse();
                    serverResponse.serviceRequest(mContext, WebServices.URL_LIKE, params, new IParseListener() {
                        @Override
                        public void ErrorResponse(VolleyError error, int requestCode) {
                            mHomeFragment.hideLoadingDialog(mContext);
                        }

                        @Override
                        public void SuccessResponse(String response, int requestCode) {
                            mHomeFragment.hideLoadingDialog(mContext);
                            if (response != null) {
                                try {
                                    JSONObject mJsonObject = new JSONObject(response);
                                    String message = mJsonObject.getString("message");
                                    if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                                        holder.mImgLike.setVisibility(View.GONE);
                                        holder.mImgUnLike.setVisibility(View.VISIBLE);
                                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }, WsUtils.WS_CODE_LIKE);
                } else {
                    PopUtils.alertDialog(mContext, mContext.getString(R.string.pls_check_internet), null);
                }
            }
        });

        /*For Menu functionality*/
        holder.mImgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialogList.clear();
                if (UserDetails.getInstance(mContext).getUserId().equalsIgnoreCase(mHomeModelList.get(position).receiverId + "")) {
                    mDialogList.add(new DialogList("", "DELETE"));
                } else {
                    mDialogList.add(new DialogList("", "REPORT"));
                }
                PopUtils.alertDialogList(mContext, mDialogList, new DialogListInterface() {
                    @Override
                    public void DialogListInterface(String id, String value) {
                        if (value.equalsIgnoreCase("REPORT")) {
                            if (PopUtils.checkInternetConnection(mContext)) {
                                requestForReportWS(position);
                            } else {
                                PopUtils.alertDialog(mContext, mContext.getString(R.string.pls_check_internet), null);
                            }
                        } else if (value.equalsIgnoreCase("DELETE")) {
                            if (PopUtils.checkInternetConnection(mContext)) {
                                requestForDeleteWS(position);
                            } else {
                                PopUtils.alertDialog(mContext, mContext.getString(R.string.pls_check_internet), null);
                            }
                        }
                    }
                });
            }
        });

        /*For Seeing User Profile Deatails*/
        holder.mTxtUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                if (UserDetails.getInstance(mContext).getUserId().equalsIgnoreCase(mHomeModelList.get(position).receiverId + "")) {
                    intent = new Intent(mContext, MyProfileActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    mContext.startActivity(intent);
                } else {
                    intent = new Intent(mContext, ProfileActivity.class);
                    intent.putExtra("USERID", mHomeModelList.get(position).receiverId + "");
                    intent.putExtra("FROM", "HOME_FRAGMENT");
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    mContext.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mHomeModelList.size();
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        LinearLayout mLlLike;
        TextView mTxtQuestion, mTxtTime, mTxtUserName, mTxtAnswer;
        ImageView mImgUser, mImgQuestion, mImgLike, mImgUnLike, mImgMenu;

        public VideoHolder(View view) {
            super(view);
            mTxtQuestion = (TextView) view.findViewById(R.id.txtQuestion);
            mTxtTime = (TextView) view.findViewById(R.id.txtTime);
            mTxtUserName = (TextView) view.findViewById(R.id.txtUserName);
            mTxtAnswer = (TextView) view.findViewById(R.id.txtAnswer);
            mImgUser = (ImageView) view.findViewById(R.id.imgUser);
            mImgQuestion = (ImageView) view.findViewById(R.id.imgQuestion);
            mImgLike = (ImageView) view.findViewById(R.id.imgLike);
            mImgUnLike = (ImageView) view.findViewById(R.id.imgUnLike);
            mImgMenu = (ImageView) view.findViewById(R.id.imgMenu);
            mLlLike = (LinearLayout) view.findViewById(R.id.llLike);
        }
    }

    public void requestForReportWS(int position) {
        mHomeFragment.showLoadingDialog(mContext, mContext.getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(mContext).getUserId());
        params.put("spamid", mHomeModelList.get(position).receiverId + "");
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(mContext, WebServices.URL_REPORT, params, new IParseListener() {
            @Override
            public void ErrorResponse(VolleyError error, int requestCode) {
                mHomeFragment.hideLoadingDialog(mContext);
            }

            @Override
            public void SuccessResponse(String response, int requestCode) {
                mHomeFragment.hideLoadingDialog(mContext);
                responseForReport(response);
            }
        }, WsUtils.WS_CODE_REPORT);
    }

    private void requestForDeleteWS(final int position) {
        mHomeFragment.showLoadingDialog(mContext, mContext.getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(mContext).getUserId());
        params.put("question_id", mHomeModelList.get(position).questionId + "");
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(mContext, WebServices.URL_DELETE, params, new IParseListener() {
            @Override
            public void ErrorResponse(VolleyError error, int requestCode) {
                mHomeFragment.hideLoadingDialog(mContext);
            }

            @Override
            public void SuccessResponse(String response, int requestCode) {
                mHomeFragment.hideLoadingDialog(mContext);
                if (response != null) {
                    try {
                        JSONObject mJsonObject = new JSONObject(response);
                        String message = mJsonObject.getString("message");
                        if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                            mHomeModelList.remove(position);
                            notifyDataSetChanged();
                        } else {
                            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, WsUtils.WS_CODE_DELETE);
    }

    private void responseForReport(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    Toast.makeText(mContext, "changes will be applied once you refresh the screen.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}




