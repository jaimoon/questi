package com.versatilemobitech.questi.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.activities.LikedAnswersActivity;
import com.versatilemobitech.questi.interfaces.DialogListInterface;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.models.DialogList;
import com.versatilemobitech.questi.models.LikedAnswers;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Excentd11 on 8/9/2017.
 */

public class LikedAnswersAdapter extends RecyclerView.Adapter<LikedAnswersAdapter.VideoHolder> implements RecyclerView.OnItemTouchListener {
    private Context mContext;
    private LikedAnswersAdapter.OnItemClickListener mListener;
    private GestureDetector mGestureDetector;
    private ArrayList<LikedAnswers> mLikedAnswers;
    private LikedAnswersActivity mLikedAnswersActivity;
    private ArrayList<DialogList> mDialogList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private TextView mTxtContent;

    public LikedAnswersAdapter(Context context, ArrayList<LikedAnswers> mLikedAnswers, LikedAnswersActivity likedAnswersActivity,
                               RecyclerView mRecyclerView, TextView mTxtContent) {
        mContext = context;
        this.mLikedAnswers = mLikedAnswers;
        this.mLikedAnswersActivity = likedAnswersActivity;
        this.mRecyclerView = mRecyclerView;
        this.mTxtContent = mTxtContent;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public LikedAnswersAdapter(Context context, LikedAnswersAdapter.OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public LikedAnswersAdapter.VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_liked_answers, parent, false);
        return new LikedAnswersAdapter.VideoHolder(view);
    }

    @Override
    public void onBindViewHolder(final LikedAnswersAdapter.VideoHolder holder, final int position) {
        final LikedAnswers mLikedAnswers1 = mLikedAnswers.get(position);

        holder.mTxtQuestion.setText(mLikedAnswers1.question);
        holder.mTxtUserName.setText(mLikedAnswers1.userName);
        holder.mTxtTime.setText(mLikedAnswers1.postedDate);

        if (mLikedAnswers1.answerImage.equalsIgnoreCase("")) {
            holder.mImgQuestion.setVisibility(View.GONE);
        } else {
            holder.mImgQuestion.setVisibility(View.VISIBLE);
            Picasso.with(mContext).load(mLikedAnswers1.answerImage).placeholder(R.drawable.user_placeholder)
                    .error(R.drawable.user_placeholder).into(holder.mImgQuestion);
        }

        if (mLikedAnswers1.answerText.equalsIgnoreCase("")) {
            holder.mTxtAnswer.setVisibility(View.GONE);
        } else {
            holder.mTxtAnswer.setVisibility(View.VISIBLE);
            holder.mTxtAnswer.setText(mLikedAnswers1.answerText);
        }

        if (mLikedAnswers1.profilePic.equalsIgnoreCase("")) {
            Picasso.with(mContext).load(R.drawable.user_placeholder).placeholder(R.drawable.user_placeholder)
                    .error(R.drawable.user_placeholder).into(holder.mImgUser);
        } else {
            Picasso.with(mContext).load(mLikedAnswers1.profilePic).placeholder(R.drawable.user_placeholder)
                    .error(R.drawable.user_placeholder).into(holder.mImgUser);
        }

        if (!UserDetails.getInstance(mContext).getUserId().equalsIgnoreCase(mLikedAnswers1.spamId + "")) {
            holder.mImgMenu.setVisibility(View.VISIBLE);
        } else {
            holder.mImgMenu.setVisibility(View.GONE);
        }

        /*For Like functionality*/
        holder.mImgLike.setVisibility(View.VISIBLE);
        holder.mImgUnLike.setVisibility(View.GONE);

        holder.mImgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PopUtils.checkInternetConnection(mContext)) {
                    mLikedAnswersActivity.showLoadingDialog(mContext.getString(R.string.loading), false);
                    HashMap<String, String> params = new HashMap<>();
                    params.put("userid", UserDetails.getInstance(mContext).getUserId());
                    params.put("question_id", mLikedAnswers1.questionId + "");
                    ServerResponse serverResponse = new ServerResponse();
                    serverResponse.serviceRequest(mContext, WebServices.URL_LIKE, params, new IParseListener() {
                        @Override
                        public void ErrorResponse(VolleyError error, int requestCode) {
                            mLikedAnswersActivity.hideLoadingDialog();
                        }

                        @Override
                        public void SuccessResponse(String response, int requestCode) {
                            mLikedAnswersActivity.hideLoadingDialog();
                            if (response != null) {
                                try {
                                    JSONObject mJsonObject = new JSONObject(response);
                                    String message = mJsonObject.getString("message");
                                    if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                                        holder.mImgLike.setVisibility(View.GONE);
                                        holder.mImgUnLike.setVisibility(View.VISIBLE);
                                        mLikedAnswers.remove(position);
                                        notifyDataSetChanged();
                                        if (mLikedAnswers.size() == 0) {
                                            mRecyclerView.setVisibility(View.GONE);
                                            mTxtContent.setVisibility(View.VISIBLE);
                                        }
                                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }, WsUtils.WS_CODE_LIKE);
                } else {
                    PopUtils.alertDialog(mContext, mContext.getString(R.string.pls_check_internet), null);
                }
            }
        });

        /*For Menu functionality*/
        holder.mImgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialogList.clear();
                mDialogList.add(new DialogList("", "REPORT"));
                PopUtils.alertDialogList(mContext, mDialogList, new DialogListInterface() {
                    @Override
                    public void DialogListInterface(String id, String value) {
                        if (value.equalsIgnoreCase("REPORT")) {
                            if (PopUtils.checkInternetConnection(mContext)) {
                                requestForReportWS(position);
                            } else {
                                PopUtils.alertDialog(mContext, mContext.getString(R.string.pls_check_internet), null);
                            }
                        }
                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {
        return mLikedAnswers.size();
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        LinearLayout mLlLike;
        TextView mTxtQuestion, mTxtTime, mTxtUserName, mTxtAnswer;
        ImageView mImgUser, mImgQuestion, mImgLike, mImgUnLike, mImgMenu;

        public VideoHolder(View view) {
            super(view);
            mTxtQuestion = (TextView) view.findViewById(R.id.txtQuestion);
            mTxtTime = (TextView) view.findViewById(R.id.txtTime);
            mTxtUserName = (TextView) view.findViewById(R.id.txtUserName);
            mTxtAnswer = (TextView) view.findViewById(R.id.txtAnswer);
            mImgUser = (ImageView) view.findViewById(R.id.imgUser);
            mImgQuestion = (ImageView) view.findViewById(R.id.imgQuestion);
            mImgLike = (ImageView) view.findViewById(R.id.imgLike);
            mImgUnLike = (ImageView) view.findViewById(R.id.imgUnLike);
            mImgMenu = (ImageView) view.findViewById(R.id.imgMenu);
            mLlLike = (LinearLayout) view.findViewById(R.id.llLike);
        }
    }

    public void requestForReportWS(int position) {
        mLikedAnswersActivity.showLoadingDialog(mContext.getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(mContext).getUserId());
        params.put("spamid", mLikedAnswers.get(position).spamId + "");
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(mContext, WebServices.URL_REPORT, params, new IParseListener() {
            @Override
            public void ErrorResponse(VolleyError error, int requestCode) {
                mLikedAnswersActivity.hideLoadingDialog();
            }

            @Override
            public void SuccessResponse(String response, int requestCode) {
                mLikedAnswersActivity.hideLoadingDialog();
                responseForReport(response);
            }
        }, WsUtils.WS_CODE_REPORT);
    }

    private void responseForReport(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}




