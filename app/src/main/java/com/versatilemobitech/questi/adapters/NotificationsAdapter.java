package com.versatilemobitech.questi.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.fragments.NotificationFragment;
import com.versatilemobitech.questi.models.Notifications;

import java.util.ArrayList;

/**
 * Created by Excentd11 on 8/9/2017.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.VideoHolder> implements RecyclerView.OnItemTouchListener {
    private Context mContext;
    private NotificationsAdapter.OnItemClickListener mListener;
    private GestureDetector mGestureDetector;
    private ArrayList<Notifications> mNotifications;
    private NotificationFragment mNotificationFragment;

    public NotificationsAdapter(Context context, ArrayList<Notifications> mNotifications, NotificationFragment notificationFragment) {
        mContext = context;
        this.mNotifications = mNotifications;
        this.mNotificationFragment = notificationFragment;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public NotificationsAdapter(Context context, NotificationsAdapter.OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public NotificationsAdapter.VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        return new NotificationsAdapter.VideoHolder(view);
    }

    @Override
    public void onBindViewHolder(final NotificationsAdapter.VideoHolder holder, final int position) {
        Notifications mNotifications1 = mNotifications.get(position);

        holder.mTxtUserName.setText(mNotifications1.userName + " answered your question");
        if (!mNotifications1.profilePic.equalsIgnoreCase("")) {
            Picasso.with(mContext).load(mNotifications1.profilePic).error(R.drawable.profile_placeholder)
                    .placeholder(R.drawable.profile_placeholder).into(holder.mImgUser);
        } else {
            Picasso.with(mContext).load(R.drawable.profile_placeholder).into(holder.mImgUser);
        }

//        if (mNotifications1.status.equalsIgnoreCase("1")) {
//            holder.mTxtUserName.setTextColor(mContext.getColor(R.color.silver));
//        } else {
//            holder.mTxtUserName.setTextColor(mContext.getColor(R.color.black));
//        }
    }

    @Override
    public int getItemCount() {
        return mNotifications.size();
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        TextView mTxtUserName;
        ImageView mImgUser;

        public VideoHolder(View view) {
            super(view);
            mTxtUserName = (TextView) view.findViewById(R.id.txtUserName);
            mImgUser = (ImageView) view.findViewById(R.id.imgUser);
        }
    }
}




