package com.versatilemobitech.questi.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.fragments.AnswerFragment;
import com.versatilemobitech.questi.fragments.QuestionsFragment;
import com.versatilemobitech.questi.interfaces.DialogListInterface;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.models.DialogList;
import com.versatilemobitech.questi.models.Questions;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Excentd12 on 7/24/2017.
 */

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.VideoHolder> implements RecyclerView.OnItemTouchListener {
    private Context mContext;
    private QuestionsAdapter.OnItemClickListener mListener;
    private GestureDetector mGestureDetector;
    private ArrayList<Questions> mQuestionsModelList;
    private QuestionsFragment mQuestionsFragment;
    private RecyclerView mRecyclerView;
    private TextView mTxtContent;
    private ArrayList<DialogList> mDialogList = new ArrayList<>();

    public QuestionsAdapter(Context context, ArrayList<Questions> mQuestionsModelList, QuestionsFragment questionsFragment,
                            RecyclerView mRecyclerView, TextView mTxtContent) {
        mContext = context;
        this.mQuestionsModelList = mQuestionsModelList;
        this.mQuestionsFragment = questionsFragment;
        this.mRecyclerView = mRecyclerView;
        this.mTxtContent = mTxtContent;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX() + 100, e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public QuestionsAdapter(Context context, QuestionsAdapter.OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public QuestionsAdapter.VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_questions, parent, false);
        return new QuestionsAdapter.VideoHolder(view);
    }

    @Override
    public void onBindViewHolder(final QuestionsAdapter.VideoHolder holder, final int position) {
        final Questions mQuestionsModel = mQuestionsModelList.get(position);
        holder.setIsRecyclable(false);
        holder.mTxtQuestion.setText(mQuestionsModel.question);
        holder.mTxtContent.setText(mQuestionsModel.postedDate);
        holder.mImgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialogList.clear();
                mDialogList.add(new DialogList("", "REPORT"));
                mDialogList.add(new DialogList("", "DELETE"));
                PopUtils.alertDialogList(mContext, mDialogList, new DialogListInterface() {
                    @Override
                    public void DialogListInterface(String id, String value) {
                        if (value.equalsIgnoreCase("REPORT")) {
                            if (PopUtils.checkInternetConnection(mContext)) {
                                requestForReportWS(position);
                            } else {
                                PopUtils.alertDialog(mContext, mContext.getString(R.string.pls_check_internet), null);
                            }
                        } else if (value.equalsIgnoreCase("DELETE")) {
                            if (PopUtils.checkInternetConnection(mContext)) {
                                requestForDeleteWS(position);
                            } else {
                                PopUtils.alertDialog(mContext, mContext.getString(R.string.pls_check_internet), null);
                            }
                        }
                    }
                });
            }
        });

        holder.mLlData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle mBundle = new Bundle();
                mBundle.putInt("QUESTIONID", mQuestionsModelList.get(position).questionId);
                mBundle.putString("QUESTION", mQuestionsModelList.get(position).question);
                navigateFragment(new AnswerFragment(), "AnswerFragment", mBundle, (FragmentActivity) mContext);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mQuestionsModelList.size();
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        LinearLayout mLlData;
        TextView mTxtQuestion, mTxtContent;
        ImageView mImgMenu;

        public VideoHolder(View view) {
            super(view);
            mTxtQuestion = (TextView) view.findViewById(R.id.txtQuestion);
            mTxtContent = (TextView) view.findViewById(R.id.txtContent);
            mImgMenu = (ImageView) view.findViewById(R.id.imgMenu);
            mLlData = (LinearLayout) view.findViewById(R.id.llData);
        }
    }

    private void requestForReportWS(int position) {
        mQuestionsFragment.showLoadingDialog(mContext, mContext.getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(mContext).getUserId());
        params.put("spamid", mQuestionsModelList.get(position).senderId + "");
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(mContext, WebServices.URL_REPORT, params, new IParseListener() {
            @Override
            public void ErrorResponse(VolleyError error, int requestCode) {
                mQuestionsFragment.hideLoadingDialog(mContext);
            }

            @Override
            public void SuccessResponse(String response, int requestCode) {
                mQuestionsFragment.hideLoadingDialog(mContext);
                responseForReport(response);
            }
        }, WsUtils.WS_CODE_REPORT);
    }

    private void requestForDeleteWS(final int position) {
        mQuestionsFragment.showLoadingDialog(mContext, mContext.getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(mContext).getUserId());
        params.put("question_id", mQuestionsModelList.get(position).questionId + "");
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(mContext, WebServices.URL_DELETE, params, new IParseListener() {
            @Override
            public void ErrorResponse(VolleyError error, int requestCode) {
                mQuestionsFragment.hideLoadingDialog(mContext);
            }

            @Override
            public void SuccessResponse(String response, int requestCode) {
                mQuestionsFragment.hideLoadingDialog(mContext);
                responseForDelete(response, position);
            }
        }, WsUtils.WS_CODE_DELETE);
    }

    private void responseForReport(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void responseForDelete(String response, int position) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    mQuestionsModelList.remove(position);
                    notifyDataSetChanged();
                    if (mQuestionsModelList.size() == 0) {
                        mRecyclerView.setVisibility(View.GONE);
                        mTxtContent.setVisibility(View.VISIBLE);
                    } else {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mTxtContent.setVisibility(View.GONE);
                    }
                    Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static void navigateFragment(Fragment fragment, String tag, Bundle bundle, FragmentActivity fragmentActivity) {
        FragmentManager fragmentManager = fragmentActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        fragmentTransaction.replace(R.id.container, fragment, tag);
        if (tag != null) {
            fragmentTransaction.addToBackStack(tag);
        }
        fragmentTransaction.commit();
    }
}




