package com.versatilemobitech.questi.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.fragments.SearchFragment;
import com.versatilemobitech.questi.models.Search;

import java.util.ArrayList;

/**
 * Created by Excentd12 on 7/26/2017.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.VideoHolder> implements RecyclerView.OnItemTouchListener,
        Filterable {
    private Context mContext;
    private SearchAdapter.OnItemClickListener mListener;
    private GestureDetector mGestureDetector;
    private ArrayList<Search> mSearchModelList;
    private SearchFragment mSearchFragment;
    private SearchFragment.CustomFilter mFilter;

    public SearchAdapter(Context context, ArrayList<Search> mSearchModelList, SearchFragment searchFragment) {
        mContext = context;
        this.mSearchModelList = mSearchModelList;
        this.mSearchFragment = searchFragment;
        mFilter = new SearchFragment.CustomFilter(SearchAdapter.this);
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
    }

    @Override
    public Filter getFilter() {
        return (mFilter);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public SearchAdapter(Context context, SearchAdapter.OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public SearchAdapter.VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search, parent, false);
        return new SearchAdapter.VideoHolder(view);
    }

    @Override
    public void onBindViewHolder(final SearchAdapter.VideoHolder holder, final int position) {
        Search mSearchModel = mSearchModelList.get(position);
        holder.mTxtUserName.setText(mSearchModel.userName);
        if (!mSearchModel.profilePic.equalsIgnoreCase("")) {
            Picasso.with(mContext).load(mSearchModel.profilePic).error(R.drawable.user_placeholder)
                    .placeholder(R.drawable.user_placeholder).into(holder.mImgProfile);
        }
    }

    @Override
    public int getItemCount() {
        return mSearchModelList.size();
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        LinearLayout mLlLike;
        TextView mTxtUserName;
        ImageView mImgProfile;

        public VideoHolder(View view) {
            super(view);
            mLlLike = (LinearLayout) view.findViewById(R.id.llLike);
            mTxtUserName = (TextView) view.findViewById(R.id.txtUserName);
            mImgProfile = (ImageView) view.findViewById(R.id.imgProfile);
        }
    }
}




