package com.versatilemobitech.questi.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.models.UserQuestions;

import java.util.ArrayList;

/**
 * Created by Excentd11 on 8/16/2017.
 */

public class UserQuestionsAdapter extends RecyclerView.Adapter<UserQuestionsAdapter.VideoHolder> implements RecyclerView.OnItemTouchListener {
    private Context mContext;
    private UserQuestionsAdapter.OnItemClickListener mListener;
    private GestureDetector mGestureDetector;
    private ArrayList<UserQuestions> mUserQuestionsList;
    private String profilePic = "";

    public UserQuestionsAdapter(Context context, ArrayList<UserQuestions> mUserQuestionsList, String profilePic) {
        mContext = context;
        this.mUserQuestionsList = mUserQuestionsList;
        this.profilePic = profilePic;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public UserQuestionsAdapter(Context context, UserQuestionsAdapter.OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public UserQuestionsAdapter.VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_questions, parent, false);
        return new UserQuestionsAdapter.VideoHolder(view);
    }

    @Override
    public void onBindViewHolder(final UserQuestionsAdapter.VideoHolder holder, final int position) {
        final UserQuestions mUserQuestions = mUserQuestionsList.get(position);

        holder.mTxtQuestion.setText(mUserQuestions.question);

        holder.mTxtLikes.setText(mUserQuestions.likes + "");

        if (!mUserQuestions.answerText.equalsIgnoreCase("")) {
            holder.mTxtAnswer.setVisibility(View.VISIBLE);
            holder.mTxtAnswer.setText(mUserQuestions.answerText);
        } else {
            holder.mTxtAnswer.setVisibility(View.GONE);
        }

        if (!mUserQuestions.answerImage.equalsIgnoreCase(" ")) {
            holder.mImgAnswer.setVisibility(View.VISIBLE);
            Picasso.with(mContext).load(mUserQuestions.answerImage).error(R.drawable.answer_placeholder)
                    .placeholder(R.drawable.answer_placeholder).into(holder.mImgAnswer);
        } else {
            holder.mImgAnswer.setVisibility(View.GONE);
        }

        if (!profilePic.equalsIgnoreCase("")) {
            Picasso.with(mContext).load(profilePic).error(R.drawable.profile_placeholder)
                    .placeholder(R.drawable.profile_placeholder).into(holder.mImgUser);
        } else {
            Picasso.with(mContext).load(R.drawable.user_placeholder).into(holder.mImgUser);
        }

    }

    @Override
    public int getItemCount() {
        return mUserQuestionsList.size();
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        TextView mTxtQuestion, mTxtAnswer, mTxtLikes;
        ImageView mImgAnswer, mImgUser;

        public VideoHolder(View view) {
            super(view);
            mTxtQuestion = (TextView) view.findViewById(R.id.txtQuestion);
            mTxtAnswer = (TextView) view.findViewById(R.id.txtAnswer);
            mTxtLikes = (TextView) view.findViewById(R.id.txtLikes);
            mImgAnswer = (ImageView) view.findViewById(R.id.imgAnswer);
            mImgUser = (ImageView) view.findViewById(R.id.imgUser);
        }
    }
}




