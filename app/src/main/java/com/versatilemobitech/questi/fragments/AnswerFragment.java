package com.versatilemobitech.questi.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.activities.CroppingActivity;
import com.versatilemobitech.questi.activities.HomeActivity;
import com.versatilemobitech.questi.interfaces.CropImageInterface;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by shivangi on 23-07-2017.
 */

public class AnswerFragment extends BaseFragment implements View.OnClickListener, IParseListener, CropImageInterface {
    private View view;
    private TextView mTxtQuestion, mTxtUploadVideo, mTxtUploadImage, mTxtSend;
    private EditText mEdtAnswer;
    private ImageView mImgUser, mImgAnswer;
    private String answerImage = "", question = "";
    private Bitmap mBitmap = null;
    private int questionId;
    private String profileImageString = "";
    String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

    private static CropImageInterface mCropImageInterface;

    public static CropImageInterface getInstance() {
        return mCropImageInterface;
    }

    public static AnswerFragment newInstance() {
        AnswerFragment fragment = new AnswerFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_answer, container, false);

        ((HomeActivity) getActivity()).mImgLogo.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mImgBack.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mImgNotification.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mEdtSearch.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).btn_search.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mTxtTitle.setVisibility(View.VISIBLE);
        mCropImageInterface = this;

        initComponents();
        return view;
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
        getBundleData();
    }

    private void setReferences() {
        mTxtQuestion = (TextView) view.findViewById(R.id.txtQuestion);
        mTxtUploadVideo = (TextView) view.findViewById(R.id.txtUploadVideo);
        mTxtUploadImage = (TextView) view.findViewById(R.id.txtUploadImage);
        mTxtSend = (TextView) view.findViewById(R.id.txtSend);
        mEdtAnswer = (EditText) view.findViewById(R.id.edtAnswer);

        mImgUser = (ImageView) view.findViewById(R.id.imgUser);
        Picasso.with(getActivity()).load(UserDetails.getInstance(getActivity()).getUserProfileImage()).error(R.drawable.profile_placeholder)
                .placeholder(R.drawable.profile_placeholder).into(mImgUser);

        mImgAnswer = (ImageView) view.findViewById(R.id.imgAnswer);
    }

    private void setClickListeners() {
        mTxtUploadVideo.setOnClickListener(this);
        mTxtUploadImage.setOnClickListener(this);
        mTxtSend.setOnClickListener(this);
    }

    private void getBundleData() {
        if (getArguments() != null) {
            questionId = getArguments().getInt("QUESTIONID");

            question = getArguments().getString("QUESTION");
            mTxtQuestion.setText(question);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtUploadVideo:
                if (!answerImage.equalsIgnoreCase("")) {
                    PopUtils.twoButtonDialog(getActivity(), getString(R.string.if_u_want_to_add_video_as_an_answer_then_pls_remove_image),
                            "YES", "NO", deleteImageClick, null);
                } else {
                    PopUtils.oneEditBoxDialog(getActivity(), getString(R.string.upload_video), getString(R.string.utube_url), uploadVideoClick);
                }
                break;
            case R.id.txtUploadImage:
                if (!PopUtils.hasPermissions(getActivity(), PERMISSIONS)) {
                    requestPermissions(PERMISSIONS, 1);
                } else {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, 690);
                }

//                if (!PopUtils.hasPermissions(getActivity(), PERMISSIONS)) {
//                    requestPermissions(PERMISSIONS, 1);
//                } else {
//                    PopUtils.alertGallery(getActivity(), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            Intent photo = new Intent("android.media.action.IMAGE_CAPTURE");
//                            startActivityForResult(photo, 2);
//                        }
//                    }, new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            Intent galleryIntent = new Intent(Intent.ACTION_PICK);
//                            galleryIntent.setType("image/*");
//                            startActivityForResult(galleryIntent, 1);
//                        }
//                    }, null);
//                }
                break;
            case R.id.txtSend:
                if (!checkValidation().equalsIgnoreCase("")) {
                    PopUtils.alertDialog(getActivity(), checkValidation(), null);
                } else {
                    if (PopUtils.checkInternetConnection(getActivity())) {
                        requestForPostAnswerWS();
                    } else {
                        PopUtils.alertDialog(getActivity(), getString(R.string.pls_check_internet), null);
                    }
                }
                break;
            default:
                break;
        }
    }

    public String checkValidation() {
        ArrayList<String> mBanWords = new ArrayList<>();
        mBanWords.add("shit");
        mBanWords.add("fuck");
        mBanWords.add("bitch");
        mBanWords.add("piss");
        mBanWords.add("dick");
        mBanWords.add("cock");
        mBanWords.add("fag");
        mBanWords.add("pussy");
        mBanWords.add("asshole");
        mBanWords.add("bastard");
        mBanWords.add("slut");
        mBanWords.add("fat");
        mBanWords.add("douche");
        mBanWords.add("bloody");
        mBanWords.add("portly");
        mBanWords.add("bugger");
        mBanWords.add("arsehole");
        mBanWords.add("ugly");
        mBanWords.add("nigga");
        mBanWords.add("overweight");
        mBanWords.add("hell");
        mBanWords.add("whore");
        mBanWords.add("puta");
        mBanWords.add("tit");
        mBanWords.add("tits");
        mBanWords.add("retard");
        mBanWords.add("beggar");
        mBanWords.add("gay");
        mBanWords.add("obese");
        mBanWords.add("fugly");
        mBanWords.add("twat");
        mBanWords.add("thot");
        mBanWords.add("wanker");
        mBanWords.add("cumdumpster");
        mBanWords.add("fuckface");
        mBanWords.add("shite");
        mBanWords.add("nutsack");
        mBanWords.add("arsewipe");
        mBanWords.add("asswipe");
        mBanWords.add("clusterfuck");

        String message = "";
        if ((TextUtils.isEmpty(mEdtAnswer.getText().toString().trim()) && TextUtils.isEmpty(profileImageString))) {
            message = getString(R.string.pls_select_atleast_one_option_for_posting_answer);
        } else {
            String s = mEdtAnswer.getText().toString().trim();
            String[] words = s.split("\\W+");

            for (int i = 0; i < mBanWords.size(); i++) {
                String mBanWord = mBanWords.get(i);
                for (int j = 0; j < words.length; j++) {
                    if (words[j].matches(mBanWord)) {
                        message = getString(R.string.bad_words_alert_for_question);
                    }
                }
            }
        }
        return message;
    }

    View.OnClickListener deleteImageClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mImgAnswer.setVisibility(View.GONE);
            answerImage = "";
            PopUtils.oneEditBoxDialog(getActivity(), getString(R.string.upload_video), getString(R.string.utube_url), uploadVideoClick);
        }
    };

    View.OnClickListener uploadVideoClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    PopUtils.alertGallery(getActivity(), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent photo = new Intent("android.media.action.IMAGE_CAPTURE");
                            startActivityForResult(photo, 2);
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                            galleryIntent.setType("image/*");
                            startActivityForResult(galleryIntent, 1);
                        }
                    }, null);
                } else {
                    Toast.makeText(getActivity(), getString(R.string.permission_denied_for_gallery), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImageUri = data.getData();

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 9;

                    String selectedImagePath = PopUtils.getPath(getActivity(), selectedImageUri);
                    Bitmap bitmap = BitmapFactory.decodeFile(selectedImagePath, options);

                    ExifInterface exif = null;
                    try {
                        exif = new ExifInterface(selectedImagePath);
                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                        mBitmap = rotateBitmap(bitmap, orientation);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (mBitmap != null) {
                        answerImage = Base64.encodeToString(PopUtils.getBytesFromBitmap(mBitmap), Base64.NO_WRAP);
                        mImgAnswer.setImageBitmap(mBitmap);
                        mImgAnswer.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case 2:
                if (data != null) {
                    mBitmap = (Bitmap) data.getExtras().get("data");
                    if (mBitmap != null) {
                        answerImage = Base64.encodeToString(PopUtils.getBytesFromBitmap(mBitmap), Base64.NO_WRAP);
                        mImgAnswer.setImageBitmap(mBitmap);
                        mImgAnswer.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case 690:
                if (data != null) {
                    Uri selectedImageUri = data.getData();
                    try {
                        Intent intent = new Intent(getActivity(), CroppingActivity.class);
                        intent.putExtra("image_path", PopUtils.getRealPathFromURI(getActivity(), selectedImageUri));
                        intent.putExtra("FROM", "ANSWER");
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            default:
                break;
        }
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {
        try {
            Matrix matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_NORMAL:
                    return bitmap;
                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                    matrix.setScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                    matrix.setRotate(180);
                    matrix.postScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_TRANSPOSE:
                    matrix.setRotate(90);
                    matrix.postScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_TRANSVERSE:
                    matrix.setRotate(-90);
                    matrix.postScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.setRotate(-90);
                    break;
                default:
                    return bitmap;
            }
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    private void requestForPostAnswerWS() {
        showLoadingDialog(getActivity(), getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("question_id", questionId + "");
        params.put("answer_text", mEdtAnswer.getText().toString());
        params.put("answer_video", "");
        if (!profileImageString.equalsIgnoreCase("")) {
            params.put("answer_image", profileImageString);
        }
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(getActivity(), WebServices.URL_POST_ANSWER, params, this, WsUtils.WS_CODE_POST_ANSWER);
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        hideLoadingDialog(getActivity());
        PopUtils.alertDialog(getActivity(), getString(R.string.something_wrong), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        hideLoadingDialog(getActivity());
        switch (requestCode) {
            case WsUtils.WS_CODE_POST_ANSWER:
                responseForPostAnswer(response);
                break;
            default:
                break;
        }
    }

    private void responseForPostAnswer(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    ((HomeActivity) getActivity()).onBackPressed();
                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void cropImageInterface(Bitmap bitmap, String path) {
        if (bitmap != null) {
            mImgAnswer.setImageBitmap(bitmap);
            mImgAnswer.setVisibility(View.VISIBLE);
            profileImageString = Base64.encodeToString(PopUtils.getBytesFromBitmap(bitmap), Base64.NO_WRAP);
        }
    }
}
