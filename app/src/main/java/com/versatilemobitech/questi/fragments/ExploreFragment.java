package com.versatilemobitech.questi.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.activities.HomeActivity;
import com.versatilemobitech.questi.adapters.ExploreAdapter;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.models.DialogList;
import com.versatilemobitech.questi.models.Explore;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.StaticUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by shivangi on 23-07-2017.
 */

public class ExploreFragment extends BaseFragment implements IParseListener {
    private View view;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private TextView mTxtContent;
    private ArrayList<Explore> mExploreModel = new ArrayList<>();
    private ArrayList<DialogList> mDialogList = new ArrayList<>();
    private int mPageCount = 1;
    private LinearLayoutManager mLayoutManager;
    private boolean canScroll = true;

    public static ExploreFragment newInstance() {
        ExploreFragment fragment = new ExploreFragment();
        return fragment;
    }

    BroadcastReceiver broadcastreceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.getStringExtra("VALUE").equalsIgnoreCase("2")) {
                    if (PopUtils.checkInternetConnection(getActivity())) {
                        requestForExploreWS(true);
                    } else {
                        PopUtils.alertDialog(getActivity(), getString(R.string.pls_check_internet), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ((HomeActivity) getActivity()).onBackPressed();
                            }
                        });
                    }
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_explore, container, false);

        ((HomeActivity) getActivity()).mBottomNavigationView.getMenu().getItem(1).setChecked(true);
        ((HomeActivity) getActivity()).mImgLogo.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mImgBack.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mImgNotification.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mEdtSearch.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).btn_search.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mTxtTitle.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mTxtTitle.setText(getString(R.string.explore));

        getActivity().registerReceiver(broadcastreceiver, new IntentFilter("com.versatilemobitech.questi.REFRESH_SCREEN"));

        initComponents();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(broadcastreceiver);
    }

    private void initComponents() {
        setReferences();
        if (PopUtils.checkInternetConnection(getActivity())) {
            requestForExploreWS(true);
        } else {
            PopUtils.alertDialog(getActivity(), getString(R.string.pls_check_internet), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((HomeActivity) getActivity()).onBackPressed();
                }
            });
        }

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                    if (canScroll) {
                        mPageCount = mPageCount + 1;
                        requestForExploreWS(true);
                    }
                }

            }
        });
    }

    private void setReferences() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.app_color));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (PopUtils.checkInternetConnection(getActivity())) {
                    requestForExploreWS(false);
                } else {
                    PopUtils.alertDialog(getActivity(), getString(R.string.pls_check_internet), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ((HomeActivity) getActivity()).onBackPressed();
                        }
                    });
                }
            }
        });

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mTxtContent = (TextView) view.findViewById(R.id.txtContent);
    }

    private void requestForExploreWS(boolean showLoading) {
        if (showLoading) {
            ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.VISIBLE);
//            showLoadingDialog(getActivity(), getString(R.string.loading), false);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(getActivity()).getUserId());
        params.put("page", mPageCount + "");
        params.put("no_of_records", StaticUtils.PAGERECORDS + "");
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(getActivity(), WebServices.URL_EXPLORE, params, this, WsUtils.WS_CODE_EXPLORE);
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.GONE);
//        hideLoadingDialog(getActivity());
        if (mPageCount == 1) {
            PopUtils.alertDialog(getActivity(), getString(R.string.something_wrong), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
        }
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.GONE);
//        hideLoadingDialog(getActivity());
        switch (requestCode) {
            case WsUtils.WS_CODE_EXPLORE:
                responseForExplore(response);
                break;
            default:
                break;
        }
    }

    private void responseForExplore(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    if (mPageCount == 1) {
                        mExploreModel.clear();
                    }
                    mSwipeRefreshLayout.setRefreshing(false);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mTxtContent.setVisibility(View.GONE);

                    JSONArray mDataArray = mJsonObject.getJSONArray("data1");
                    for (int i = 0; i < mDataArray.length(); i++) {
                        JSONObject mDataObject = mDataArray.getJSONObject(i);

                        mExploreModel.add(new Explore(mDataObject.optString("Profile Pic"), mDataObject.optString("Username"),
                                mDataObject.optString("Question"), mDataObject.optString("Answer text"), mDataObject.optString("Answer image"),
                                mDataObject.optString("Answer video"), mDataObject.optString("Posted date"), mDataObject.optInt("Question_id"),
                                mDataObject.optInt("Total Likes"), mDataObject.optInt("Your like"), mDataObject.optInt("receiver_id")));
                    }

                    setAdapter();

                    if (mPageCount != 1) {
                        int itemPosition = ((mPageCount - 1) * StaticUtils.PAGERECORDS + 1);
                        mRecyclerView.scrollToPosition(itemPosition);
                    }

                } else {
                    if (mPageCount == 1) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        mTxtContent.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    } else {
                        canScroll = false;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setAdapter() {
        mLayoutManager = new LinearLayoutManager(getContext());
        ExploreAdapter mExploreAdapter = new ExploreAdapter(getContext(), mExploreModel,
                new ExploreFragment(), mRecyclerView, mTxtContent);
        mRecyclerView.setAdapter(mExploreAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnItemTouchListener(new ExploreAdapter(getContext(), new ExploreAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }
        }));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (data.getBooleanExtra("REFRESH_SCREEN", false)) {
                Toast.makeText(getActivity(), "success", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
