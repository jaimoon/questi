package com.versatilemobitech.questi.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.activities.HomeActivity;
import com.versatilemobitech.questi.adapters.HomeAdapter;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.models.HomeModel;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.StaticUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by shivangi on 23-07-2017.
 */

public class HomeFragment extends BaseFragment implements IParseListener {
    private View view;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TextView mTxtContent;
    private int mPageCount = 1;
    private ArrayList<HomeModel> mHomeModel = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    private boolean canScroll = true;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    BroadcastReceiver broadcastreceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.getStringExtra("VALUE").equalsIgnoreCase("1")) {
                    if (PopUtils.checkInternetConnection(getActivity())) {
                        requestForHomeWS(true);
                    } else {
                        PopUtils.alertDialog(getActivity(), getString(R.string.pls_check_internet), null);
                    }
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        ((HomeActivity) getActivity()).mBottomNavigationView.getMenu().getItem(0).setChecked(true);
        ((HomeActivity) getActivity()).mImgLogo.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mImgBack.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mImgNotification.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mEdtSearch.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).btn_search.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mTxtTitle.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mTxtTitle.setText(getString(R.string.home));

        getActivity().registerReceiver(broadcastreceiver, new IntentFilter("com.versatilemobitech.questi.REFRESH_SCREEN"));

        initComponents();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(broadcastreceiver);
    }

    private void initComponents() {
        setReferences();
        if (PopUtils.checkInternetConnection(getActivity())) {
            requestForHomeWS(true);
        } else {
            PopUtils.alertDialog(getActivity(), getString(R.string.pls_check_internet), null);
        }

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                    if (canScroll) {
                        mPageCount = mPageCount + 1;
                        requestForHomeWS(true);
                    }
                }
            }
        });
    }

    private void setReferences() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.app_color));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (PopUtils.checkInternetConnection(getActivity())) {
                    requestForHomeWS(false);
                } else {
                    PopUtils.alertDialog(getActivity(), getString(R.string.pls_check_internet), null);
                }
            }
        });

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mTxtContent = (TextView) view.findViewById(R.id.txtContent);
    }

    private void requestForHomeWS(boolean showLoading) {
        if (showLoading) {
            ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.VISIBLE);
//            showLoadingDialog(getActivity(), getString(R.string.loading), false);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(getActivity()).getUserId());
        params.put("page", mPageCount + "");
        params.put("no_of_records", StaticUtils.PAGERECORDS + "");
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(getActivity(), WebServices.URL_HOME, params, this, WsUtils.WS_CODE_HOME);
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        mSwipeRefreshLayout.setRefreshing(false);
        ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.GONE);
        hideLoadingDialog(getActivity());
        if (mPageCount == 1) {
            PopUtils.alertDialog(getActivity(), getString(R.string.something_wrong), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
        }
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.GONE);
        hideLoadingDialog(getActivity());
        switch (requestCode) {
            case WsUtils.WS_CODE_HOME:
                responseForHome(response);
                break;
            default:
                break;
        }
    }

    private void responseForHome(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    if (mPageCount == 1) {
                        mHomeModel.clear();
                    }
                    mSwipeRefreshLayout.setRefreshing(false);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mTxtContent.setVisibility(View.GONE);


                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    for (int i = 0; i < mDataArray.length(); i++) {
                        JSONObject mDataObject = mDataArray.getJSONObject(i);

                        mHomeModel.add(new HomeModel(mDataObject.optString("Profile Pic"), mDataObject.optString("Username"),
                                mDataObject.optString("Question"), mDataObject.optString("Answer text"),
                                mDataObject.optString("Answer image"), mDataObject.optString("Answer video"),
                                mDataObject.optString("Posted date"), mDataObject.optInt("Question_id"),
                                mDataObject.optInt("Total Likes"), mDataObject.optInt("Your like"),
                                mDataObject.optInt("spam id")));
                    }

                    ((HomeActivity) getActivity()).setBadgeCount(mJsonObject.optInt("count"));
                    ((HomeActivity) getActivity()).setNotificationBadgeCount(mJsonObject.optInt("notification"));

                    setAdapter();

                    if (mPageCount != 1) {
                        int itemPosition = ((mPageCount - 1) * StaticUtils.PAGERECORDS + 1);
                        mRecyclerView.scrollToPosition(itemPosition);
                    }

                } else {
                    if (mPageCount == 1) {
                        mTxtContent.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    } else {
                        canScroll = false;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setAdapter() {
        mLayoutManager = new LinearLayoutManager(getContext());
        HomeAdapter mHomeAdapter = new HomeAdapter(getContext(), mHomeModel, new HomeFragment(), mSwipeRefreshLayout);
        mRecyclerView.setAdapter(mHomeAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnItemTouchListener(new HomeAdapter(getContext(), new HomeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }
        }));
    }
}
