package com.versatilemobitech.questi.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.activities.HomeActivity;
import com.versatilemobitech.questi.adapters.NotificationsAdapter;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.models.Notifications;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Excentd11 on 8/9/2017.
 */

public class NotificationFragment extends BaseFragment implements View.OnClickListener, IParseListener {
    private View view;
    public RecyclerView mRecyclerView;
    private TextView mTxtContent;
    private NotificationsAdapter mNotificationsAdapter;
    private ArrayList<Notifications> mNotificationsList = new ArrayList<>();

    public static NotificationFragment newInstance() {
        NotificationFragment fragment = new NotificationFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notifications, container, false);

        ((HomeActivity) getActivity()).mImgLogo.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mImgBack.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mImgNotification.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mEdtSearch.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).btn_search.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mTxtTitle.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mTxtTitle.setText(getString(R.string.notifications));
        ((HomeActivity) getActivity()).setNotificationBadgeCount(0);

        initComponents();
        return view;
    }

    private void initComponents() {
        setReferences();
        setClickListeners();

        if (PopUtils.checkInternetConnection(getActivity())) {
            requestForNotificationsWS(true);
        } else {
            PopUtils.alertDialog(getActivity(), getString(R.string.pls_check_internet), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((HomeActivity) getActivity()).onBackPressed();
                }
            });
        }
    }

    private void setReferences() {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mTxtContent = (TextView) view.findViewById(R.id.txtContent);
    }

    private void setClickListeners() {
        ((HomeActivity) getActivity()).mImgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                ((HomeActivity) getActivity()).onBackPressed();
                break;
            default:
                break;
        }
    }

    private void requestForNotificationsWS(boolean showLoading) {
        if (showLoading) {
            ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.VISIBLE);
//            showLoadingDialog(getActivity(), getString(R.string.loading), false);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(getActivity()).getUserId());
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(getActivity(), WebServices.URL_MY_NOTIFICATIONS, params, this, WsUtils.WS_CODE_NOTIFICATIONS);
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.GONE);
//        hideLoadingDialog(getActivity());
        PopUtils.alertDialog(getActivity(), getString(R.string.something_wrong), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.GONE);
//        hideLoadingDialog(getActivity());
        switch (requestCode) {
            case WsUtils.WS_CODE_NOTIFICATIONS:
                responseForNotifications(response);
                break;
            default:
                break;
        }
    }

    private void responseForNotifications(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mTxtContent.setVisibility(View.GONE);

                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    for (int i = 0; i < mDataArray.length(); i++) {
                        JSONObject mDataObject = mDataArray.getJSONObject(i);

                        mNotificationsList.add(new Notifications(mDataObject.optInt("Question_id"),
                                mDataObject.optString("Profile Pic"), mDataObject.optString("Username"),
                                mDataObject.optString("Question"), mDataObject.optString("Answer text"),
                                mDataObject.optString("Answer image"), mDataObject.optString("Answer video"),
                                mDataObject.optString("status")));

                        setAdapter();
                    }
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    mTxtContent.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mNotificationsAdapter = new NotificationsAdapter(getContext(), mNotificationsList, new NotificationFragment());
        mRecyclerView.setAdapter(mNotificationsAdapter);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addOnItemTouchListener(new NotificationsAdapter(getContext(), new NotificationsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }
        }));
    }
}
