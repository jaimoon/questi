package com.versatilemobitech.questi.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.activities.HomeActivity;
import com.versatilemobitech.questi.adapters.QuestionsAdapter;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.models.DialogList;
import com.versatilemobitech.questi.models.Questions;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.StaticUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Excentd12 on 7/24/2017.
 */

public class QuestionsFragment extends BaseFragment implements View.OnClickListener, IParseListener {
    private View view;
    private TextView mTxtGetQuestion, mTxtContent, mTxtMessage;
    public RecyclerView mRecyclerView;
    private QuestionsAdapter mQuestionsAdapter;
    private ArrayList<Questions> mQuestionsModel;
    private ArrayList<DialogList> mDialogList = new ArrayList<>();
    private Context mContext;
    public Boolean initialized = false;
    private int mPageCount = 1;
    private LinearLayoutManager mLayoutManager;
    private boolean canScroll = true;

    public static QuestionsFragment newInstance() {
        QuestionsFragment fragment = new QuestionsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_questions, container, false);

        ((HomeActivity) getActivity()).mBottomNavigationView.getMenu().getItem(2).setChecked(true);
        ((HomeActivity) getActivity()).mImgLogo.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mImgBack.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mImgNotification.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mEdtSearch.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).btn_search.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mTxtTitle.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mTxtTitle.setText(getString(R.string.questions));
        ((HomeActivity) getActivity()).setBadgeCount(0);

        initComponents();
        return view;
    }

    private void initComponents() {
        mQuestionsModel = new ArrayList<>();
        setReferences();
        setClickListeners();

        if (PopUtils.checkInternetConnection(getActivity())) {
            requestForGetQuestionsWS();
        } else {
            PopUtils.alertDialog(getActivity(), getString(R.string.pls_check_internet), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((HomeActivity) getActivity()).onBackPressed();
                }
            });
        }
        initialized = true;

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                    if (canScroll) {
                        mPageCount = mPageCount + 1;
                        requestForGetQuestionsWS();
                    }
                }
            }
        });
    }

    private void setReferences() {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mTxtGetQuestion = (TextView) view.findViewById(R.id.txtGetQuestion);
        mTxtContent = (TextView) view.findViewById(R.id.txtContent);
        mTxtMessage = (TextView) view.findViewById(R.id.txtMessage);
    }

    private void setClickListeners() {
        mTxtGetQuestion.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtGetQuestion:
                if (PopUtils.checkInternetConnection(getActivity())) {
                    requestForGetQuestionsWS();
                } else {
                    PopUtils.alertDialog(getActivity(), getString(R.string.pls_check_internet), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ((HomeActivity) getActivity()).onBackPressed();
                        }
                    });
                }
                break;
            default:
                break;
        }
    }

    private void requestForGetQuestionsWS() {
        ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.VISIBLE);
//        showLoadingDialog(getActivity(), getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(getActivity()).getUserId());
        params.put("page", mPageCount + "");
        params.put("no_of_records", StaticUtils.PAGERECORDS + "");
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(getActivity(), WebServices.URL_GET_QUESTIONS, params, this, WsUtils.WS_CODE_GET_QUESTIONS);
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.GONE);
//        hideLoadingDialog(getActivity());
        if (mPageCount == 1) {
            PopUtils.alertDialog(getActivity(), getString(R.string.something_wrong), new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        switch (requestCode) {
            case WsUtils.WS_CODE_GET_QUESTIONS:
                ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.GONE);
//                hideLoadingDialog(getActivity());
                responseForGetQuestions(response);
                break;
            default:
                break;
        }
    }

    private void responseForGetQuestions(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                mTxtMessage.setText(message);
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    if (mPageCount == 1) {
                        mQuestionsModel.clear();
                    }
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mTxtContent.setVisibility(View.GONE);

                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    for (int i = 0; i < mDataArray.length(); i++) {
                        JSONObject mDataObject = mDataArray.getJSONObject(i);
                        mQuestionsModel.add(new Questions(mDataObject.optInt("question_id"), mDataObject.optInt("sender_id"),
                                mDataObject.optString("question"), mDataObject.optString("posted_date")));
                    }

                    setAdapter();

                    if (mPageCount != 1) {
                        int itemPosition = ((mPageCount - 1) * StaticUtils.PAGERECORDS + 1);
                        mRecyclerView.scrollToPosition(itemPosition);
                    }

                } else {
                    if (mPageCount == 1) {
                        mTxtContent.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    } else {
                        canScroll = false;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setAdapter() {
        mLayoutManager = new LinearLayoutManager(getContext());
        mQuestionsAdapter = new QuestionsAdapter(getContext(), mQuestionsModel, new QuestionsFragment(),
                mRecyclerView, mTxtContent);
        mRecyclerView.setAdapter(mQuestionsAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnItemTouchListener(new QuestionsAdapter(getContext(), new QuestionsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }
        }));
    }
}
