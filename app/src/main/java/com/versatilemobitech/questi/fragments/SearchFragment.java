package com.versatilemobitech.questi.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.activities.HomeActivity;
import com.versatilemobitech.questi.activities.ProfileActivity;
import com.versatilemobitech.questi.adapters.SearchAdapter;
import com.versatilemobitech.questi.interfaces.IParseListener;
import com.versatilemobitech.questi.models.Search;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;
import com.versatilemobitech.questi.webUtils.ServerResponse;
import com.versatilemobitech.questi.webUtils.WebServices;
import com.versatilemobitech.questi.webUtils.WsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by shivangi on 23-07-2017.
 */

public class SearchFragment extends BaseFragment implements View.OnClickListener, IParseListener {
    private View view;
    public RecyclerView mRecyclerView;
    private TextView mTxtContent;
    private SearchAdapter mSearchAdapter;
    private static ArrayList<Search> mSearchModel = new ArrayList<>();
    private static ArrayList<Search> mFilteredSearchModel = new ArrayList<>();


    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }

    BroadcastReceiver broadcastreceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.getStringExtra("VALUE").equalsIgnoreCase("3")) {
                    if (PopUtils.checkInternetConnection(getActivity())) {
                        requestForAllUsersWS();
                    } else {
                        PopUtils.alertDialog(getActivity(), getString(R.string.pls_check_internet), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ((HomeActivity) getActivity()).onBackPressed();
                            }
                        });
                    }
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);

        ((HomeActivity) getActivity()).mBottomNavigationView.getMenu().getItem(3).setChecked(true);
        ((HomeActivity) getActivity()).mImgLogo.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mImgBack.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mImgNotification.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mEdtSearch.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).btn_search.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mTxtTitle.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mTxtTitle.setText(getString(R.string.search_users));
        ((HomeActivity) getActivity()).setNotificationBadgeCount(0);

        getActivity().registerReceiver(broadcastreceiver, new IntentFilter("com.versatilemobitech.questi.REFRESH_SCREEN"));

        initComponents();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(broadcastreceiver);
    }

    private void initComponents() {
        setReferences();
        setClickListeners();

        if (PopUtils.checkInternetConnection(getActivity())) {
            requestForAllUsersWS();
        } else {
            PopUtils.alertDialog(getActivity(), getString(R.string.pls_check_internet), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((HomeActivity) getActivity()).onBackPressed();
                }
            });
        }
    }

    private void setReferences() {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mTxtContent = (TextView) view.findViewById(R.id.txtContent);
        //  mEdtSearch= (EditText) view.findViewById(R.id.edtSearch);

        ((HomeActivity) getActivity()).mEdtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mSearchAdapter != null) {
                    mSearchAdapter.getFilter().filter(charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setClickListeners() {
        ((HomeActivity) getActivity()).mImgBack.setOnClickListener(this);
        ((HomeActivity) getActivity()).btn_search.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                ((HomeActivity) getActivity()).onBackPressed();
                break;
            case R.id.btn_search:
                if (PopUtils.checkInternetConnection(getActivity())) {
                    requestForSearchUsersWS();
                } else {
                    PopUtils.alertDialog(getActivity(), getString(R.string.pls_check_internet), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ((HomeActivity) getActivity()).onBackPressed();
                        }
                    });
                }
                break;
            default:
                break;
        }
    }

    private void requestForSearchUsersWS() {
        ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.VISIBLE);
//        showLoadingDialog(getActivity(), getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(getActivity()).getUserId());
        params.put("search", ((HomeActivity) getActivity()).mEdtSearch.getText().toString().trim());
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(getActivity(), WebServices.URL_ALL_USERS, params, this, WsUtils.WS_CODE_All_USERS);
    }

    private void requestForAllUsersWS() {
        ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.VISIBLE);
//        showLoadingDialog(getActivity(), getString(R.string.loading), false);
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", UserDetails.getInstance(getActivity()).getUserId());
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.serviceRequest(getActivity(), WebServices.URL_ALL_USERS, params, this, WsUtils.WS_CODE_All_USERS);
    }

    @Override
    public void ErrorResponse(VolleyError error, int requestCode) {
        ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.GONE);
//        hideLoadingDialog(getActivity());
        PopUtils.alertDialog(getActivity(), getString(R.string.something_wrong), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public void SuccessResponse(String response, int requestCode) {
        ((HomeActivity) getActivity()).mLlProgressBar.setVisibility(View.GONE);
//        hideLoadingDialog(getActivity());
        switch (requestCode) {
            case WsUtils.WS_CODE_All_USERS:
                responseForAllUsers(response);
                break;
            default:
                break;
        }
    }

    private void responseForAllUsers(String response) {
        if (response != null) {
            try {
                JSONObject mJsonObject = new JSONObject(response);
                String message = mJsonObject.getString("message");
                if (mJsonObject.getString("status").equalsIgnoreCase("200")) {
                    mSearchModel.clear();
                    mFilteredSearchModel.clear();
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mTxtContent.setVisibility(View.GONE);

                    JSONArray mDataArray = mJsonObject.getJSONArray("data");
                    for (int i = 0; i < mDataArray.length(); i++) {
                        JSONObject mDataObject = mDataArray.getJSONObject(i);
                        String questionsArray = "";

                        JSONObject mQuestionsObject = mDataObject.getJSONObject("Questions");
                        if (mQuestionsObject.optString("status").equalsIgnoreCase("200")) {
                            JSONArray mQuestionsArray = mQuestionsObject.getJSONArray("data");
                            questionsArray = mQuestionsArray.toString();
                        }

                        mSearchModel.add(new Search(mDataObject.optInt("userid"), mDataObject.optInt("following"),
                                mDataObject.optInt("follower"), mDataObject.optInt("status"), mDataObject.optString("Username"),
                                mDataObject.optString("email"), mDataObject.optString("profile"), questionsArray));
                    }

                    mFilteredSearchModel.addAll(mSearchModel);
                    setAdapter();

                } else {
                    mTxtContent.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mSearchAdapter = new SearchAdapter(getContext(), mFilteredSearchModel, new SearchFragment());
        mRecyclerView.setAdapter(mSearchAdapter);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addOnItemTouchListener(new SearchAdapter(getContext(), new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent mIntent = new Intent(getActivity(), ProfileActivity.class);
                mIntent.putExtra("USERID", mSearchModel.get(position).userId + "");
                mIntent.putExtra("FROM", "SEARCH_FRAGMENT");
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mIntent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        }));
    }

    public static class CustomFilter extends Filter
    {
        private SearchAdapter mSearchAdapter;

        public CustomFilter(SearchAdapter mBidCarsAdapter)
        {
            super();
            this.mSearchAdapter = mBidCarsAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            mFilteredSearchModel.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                mFilteredSearchModel.addAll(mSearchModel);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Search mSearch : mSearchModel) {
                    if (mSearch.userName.toLowerCase().contains(filterPattern) ||
                            mSearch.emailId.toLowerCase().contains(filterPattern)) {
                        mFilteredSearchModel.add(mSearch);
                    }
                }
            }
            results.values = mFilteredSearchModel;
            results.count = mFilteredSearchModel.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (((List<Search>) results.values) != null) {
                if (((List<Search>) results.values).size() > 0) {
                    this.mSearchAdapter.notifyDataSetChanged();
                } else {

                }
            }
        }
    }
}
