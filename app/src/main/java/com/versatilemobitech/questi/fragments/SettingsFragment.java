package com.versatilemobitech.questi.fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.activities.ChangePasswordActivity;
import com.versatilemobitech.questi.activities.ContactUsActivity;
import com.versatilemobitech.questi.activities.HomeActivity;
import com.versatilemobitech.questi.activities.HowYouLookActivity;
import com.versatilemobitech.questi.activities.LikedAnswersActivity;
import com.versatilemobitech.questi.activities.LoginActivity;
import com.versatilemobitech.questi.activities.PrivacyPolicyActivity;
import com.versatilemobitech.questi.utils.PopUtils;
import com.versatilemobitech.questi.utils.UserDetails;

/**
 * Created by shivangi on 23-07-2017.
 */

public class SettingsFragment extends Fragment implements View.OnClickListener {
    private View view;
    private TextView mTxtShareProfile, mTxtFindFriends, mTxtPrivacyPolicy, mTxtLikedAnswers, mTxtChangeUsername, mTxtContactUs,
            mTxtChangeLanguage, mTxtChangePassword, mTxtLogout;
    private Intent intent;

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);

        ((HomeActivity) getActivity()).mBottomNavigationView.getMenu().getItem(4).setChecked(true);
        ((HomeActivity) getActivity()).mImgLogo.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mImgBack.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mImgNotification.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mEdtSearch.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).btn_search.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).mTxtTitle.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).mTxtTitle.setText(getString(R.string.settings));

        initComponents();
        return view;
    }

    private void initComponents() {
        setReferences();
        setClickListeners();
    }

    private void setReferences() {
        mTxtShareProfile = (TextView) view.findViewById(R.id.txtShareProfile);
        mTxtFindFriends = (TextView) view.findViewById(R.id.txtFindFriends);
        mTxtPrivacyPolicy = (TextView) view.findViewById(R.id.txtPrivacyPolicy);
        mTxtLikedAnswers = (TextView) view.findViewById(R.id.txtLikedAnswers);
        mTxtChangeUsername = (TextView) view.findViewById(R.id.txtChangeUsername);
        mTxtContactUs = (TextView) view.findViewById(R.id.txtContactUs);
        mTxtChangeLanguage = (TextView) view.findViewById(R.id.txtChangeLanguage);
        mTxtChangePassword = (TextView) view.findViewById(R.id.txtChangePassword);
        mTxtLogout = (TextView) view.findViewById(R.id.txtLogout);
    }

    private void setClickListeners() {
        mTxtShareProfile.setOnClickListener(this);
        mTxtFindFriends.setOnClickListener(this);
        mTxtPrivacyPolicy.setOnClickListener(this);
        mTxtLikedAnswers.setOnClickListener(this);
        mTxtChangeUsername.setOnClickListener(this);
        mTxtChangeLanguage.setOnClickListener(this);
        mTxtChangePassword.setOnClickListener(this);
        mTxtContactUs.setOnClickListener(this);
        mTxtLogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtShareProfile:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "https://play.google.com/store/apps/details?id=com.versatilemobitech.questi";
                String shareSub = getString(R.string.share_body);
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_title)));
                break;
            case R.id.txtFindFriends:
                break;
            case R.id.txtPrivacyPolicy:
                intent = new Intent(getActivity(), PrivacyPolicyActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                break;
            case R.id.txtLikedAnswers:
                intent = new Intent(getActivity(), LikedAnswersActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                break;
            case R.id.txtChangeUsername:
                intent = new Intent(getActivity(), HowYouLookActivity.class);
                intent.putExtra("FROM", "SETTINGS");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                break;
            case R.id.txtContactUs:
                intent = new Intent(getActivity(), ContactUsActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                break;
            case R.id.txtChangeLanguage:
                PopUtils.changeLanguageDialog(getActivity(), UserDetails.getInstance(getActivity()).getLanguage(), changeLanguageClicked);
                break;
            case R.id.txtChangePassword:
                intent = new Intent(getActivity(), ChangePasswordActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                break;
            case R.id.txtLogout:
                PopUtils.twoButtonDialog(getActivity(), getString(R.string.are_u_sure_u_want_to_logout),
                        getString(R.string.yes), getString(R.string.no), logoutClick, logoutNoClick);
                break;
        }
    }

    View.OnClickListener logoutClick = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onClick(View view) {
            UserDetails.getInstance(getActivity()).setLanguage("ENGLISH");

            UserDetails.getInstance(getActivity()).setUserId("");
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
            getActivity().finish();
            getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        }
    };

    View.OnClickListener logoutNoClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        }
    };

    View.OnClickListener changeLanguageClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            UserDetails.getInstance(getActivity()).setLanguage(view.getTag() + "");

            PopUtils.SetLocalization(getActivity(), UserDetails.getInstance(getActivity()).getLanguage());
        }
    };
}
