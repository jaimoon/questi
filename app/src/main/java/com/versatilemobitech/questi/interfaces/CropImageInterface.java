package com.versatilemobitech.questi.interfaces;

import android.graphics.Bitmap;

/**
 * Created by Excentd11 on 8/31/2017.
 */

public interface CropImageInterface {
    void cropImageInterface(Bitmap bitmap, String path);
}
