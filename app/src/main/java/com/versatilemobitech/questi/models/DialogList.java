package com.versatilemobitech.questi.models;

/**
 * Created by Excentd11 on 7/15/2017.
 */

public class DialogList {
    public String id, value;

    public DialogList(String id, String value) {
        this.id = id;
        this.value = value;
    }
}
