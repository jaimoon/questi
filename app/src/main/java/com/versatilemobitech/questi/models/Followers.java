package com.versatilemobitech.questi.models;

/**
 * Created by Excentd11 on 8/24/2017.
 */

public class Followers {
    public int userId;
    public String userName, profilePic;

    public Followers(int userId, String userName, String profilePic) {
        this.userId = userId;
        this.userName = userName;
        this.profilePic = profilePic;
    }
}
