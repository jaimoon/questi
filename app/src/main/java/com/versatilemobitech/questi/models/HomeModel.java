package com.versatilemobitech.questi.models;

/**
 * Created by Excentd11 on 8/8/2017.
 */

public class HomeModel {
    public String profilePic, userName, question, answerText, answerImage, answerVideo, postedDate;
    public int questionId, totalLikes, yourLike, receiverId;

    public HomeModel(String profilePic, String userName, String question, String answerText, String answerImage, String answerVideo,
                     String postedDate, int questionId, int totalLikes, int yourLike, int receiverId) {
        this.profilePic = profilePic;
        this.userName = userName;
        this.question = question;
        this.answerText = answerText;
        this.answerImage = answerImage;
        this.answerVideo = answerVideo;
        this.postedDate = postedDate;
        this.questionId = questionId;
        this.totalLikes = totalLikes;
        this.yourLike = yourLike;
        this.receiverId = receiverId;
    }
}
