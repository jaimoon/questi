package com.versatilemobitech.questi.models;

/**
 * Created by Excentd11 on 8/9/2017.
 */

public class LikedAnswers {
    public int questionId, spamId, totalLikes, yourLike;
    public String profilePic, userName, question, answerText, answerImage, answerVideo, postedDate;

    public LikedAnswers(int questionId, int spamId, int totalLikes, int yourLike, String profilePic, String userName,
                        String question, String answerText, String answerImage, String answerVideo, String postedDate) {
        this.questionId = questionId;
        this.spamId = spamId;
        this.totalLikes = totalLikes;
        this.yourLike = yourLike;
        this.profilePic = profilePic;
        this.userName = userName;
        this.question = question;
        this.answerText = answerText;
        this.answerImage = answerImage;
        this.answerVideo = answerVideo;
        this.postedDate = postedDate;
    }
}
