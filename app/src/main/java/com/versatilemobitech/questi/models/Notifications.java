package com.versatilemobitech.questi.models;

/**
 * Created by Excentd11 on 8/9/2017.
 */

public class Notifications {
    public int questionId;
    public String profilePic, userName, question, answerText, answerImage, answerVideo, status;

    public Notifications(int questionId, String profilePic, String userName, String question, String answerText, String answerImage,
                         String answerVideo, String status) {
        this.questionId = questionId;
        this.profilePic = profilePic;
        this.userName = userName;
        this.question = question;
        this.answerText = answerText;
        this.answerImage = answerImage;
        this.answerVideo = answerVideo;
        this.status = status;
    }
}
