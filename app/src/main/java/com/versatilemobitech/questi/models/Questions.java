package com.versatilemobitech.questi.models;

/**
 * Created by Excentd12 on 7/24/2017.
 */

public class Questions {
    public int questionId, senderId;
    public String question, postedDate;

    public Questions(int questionId, int senderId, String question, String postedDate) {
        this.questionId = questionId;
        this.senderId = senderId;
        this.question = question;
        this.postedDate = postedDate;
    }
}
