package com.versatilemobitech.questi.models;

/**
 * Created by Excentd12 on 7/26/2017.
 */

public class Search {
    public int userId, following, followers, status;
    public String userName, emailId, profilePic, questions;

    public Search(int userId, int following, int followers, int status, String userName, String emailId, String profilePic,
                  String questions) {
        this.userId = userId;
        this.following = following;
        this.followers = followers;
        this.status = status;
        this.userName = userName;
        this.emailId = emailId;
        this.profilePic = profilePic;
        this.questions = questions;
    }
}
