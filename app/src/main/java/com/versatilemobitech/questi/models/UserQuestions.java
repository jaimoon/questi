package com.versatilemobitech.questi.models;

import java.io.Serializable;

/**
 * Created by Excentd11 on 8/16/2017.
 */

public class UserQuestions implements Serializable {
    public int questionId, likes;
    public String question, answerText, answerImage, answerVideo;

    public UserQuestions(int questionId, int likes, String question, String answerText, String answerImage, String answerVideo) {
        this.questionId = questionId;
        this.likes = likes;
        this.question = question;
        this.answerText = answerText;
        this.answerImage = answerImage;
        this.answerVideo = answerVideo;
    }
}
