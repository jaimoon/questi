package com.versatilemobitech.questi.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.versatilemobitech.questi.R;
import com.versatilemobitech.questi.adapters.DialogListAdapter;
import com.versatilemobitech.questi.interfaces.DialogListInterface;
import com.versatilemobitech.questi.models.DialogList;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Excentd11 on 8/8/2017.
 */

public class PopUtils {
    public static void alertDialog(final Context mContext, String message, final View.OnClickListener okClick) {
        TextView mTxtOk, mTxtMessage;
        final Dialog dialog = new Dialog(mContext, R.style.AlertDialogCustom);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = LayoutInflater.from(mContext).inflate(R.layout.alert_dialog, null);
        mTxtOk = (TextView) v.findViewById(R.id.txtOk);
        mTxtMessage = (TextView) v.findViewById(R.id.txtMessage);

        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialogCustom;
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        mTxtMessage.setText(message);
        mTxtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (okClick != null) {
                    okClick.onClick(v);
                }
            }
        });

        dialog.setContentView(v);
        dialog.setCancelable(false);

        int width = (int) (mContext.getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (mContext.getResources().getDisplayMetrics().heightPixels * 0.30);
        dialog.getWindow().setLayout(width, lp.height);

        dialog.show();
    }

    public static void twoButtonDialog(final Context mContext, String message, String oneString, String twoString,
                                       final View.OnClickListener oneClick, final View.OnClickListener twoClick) {
        TextView mTxtOne, mTxtTwo, mTxtMessage, mTxtTitle;
        LinearLayout mLlOne, mLlTwo;
        final Dialog dialog = new Dialog(mContext, R.style.AlertDialogCustom);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = LayoutInflater.from(mContext).inflate(R.layout.alert_twobutton_dialog, null);
        mTxtOne = (TextView) v.findViewById(R.id.txtYes);
        mTxtOne.setText(oneString);
        mTxtTwo = (TextView) v.findViewById(R.id.txtNo);
        mTxtTwo.setText(twoString);
        mTxtMessage = (TextView) v.findViewById(R.id.txtMessage);
        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mLlOne = (LinearLayout) v.findViewById(R.id.llOne);
        mLlTwo = (LinearLayout) v.findViewById(R.id.llTwo);

        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialogCustom;
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        mTxtMessage.setText(message);

        mTxtOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (oneClick != null) {
                    oneClick.onClick(v);
                }
            }
        });

        mTxtTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (twoClick != null) {
                    twoClick.onClick(v);
                }
            }
        });

        dialog.setContentView(v);
        dialog.setCancelable(false);

        int width = (int) (mContext.getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (mContext.getResources().getDisplayMetrics().heightPixels * 0.30);
        dialog.getWindow().setLayout(width, lp.height);

        dialog.show();
    }


    public static void oneEditBoxDialog(final Context mContext, final String heading, final String hint,
                                        final View.OnClickListener submitClick) {
        TextView mTxtSubmit, mTxtHeading;
        final EditText mEdtMobile;
        final Dialog dialog = new Dialog(mContext, R.style.AlertDialogCustom);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = LayoutInflater.from(mContext).inflate(R.layout.alert_forgot_password, null);
        mTxtSubmit = (TextView) v.findViewById(R.id.txtSubmit);
        mTxtHeading = (TextView) v.findViewById(R.id.txtHeading);
        mTxtHeading.setText(heading);

        mEdtMobile = (EditText) v.findViewById(R.id.edtMobile);
        mEdtMobile.setHint(hint);

        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialogCustom;
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        mTxtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEdtMobile.getText().toString().trim().length() == 0) {
                    PopUtils.alertDialog(mContext, mContext.getString(R.string.please_enter) + " " + hint, null);
                } else {
                    dialog.dismiss();
                    if (submitClick != null) {
                        v.setTag(mEdtMobile.getText().toString().trim());
                        submitClick.onClick(v);
                    }
                }
            }
        });

        dialog.setContentView(v);
        dialog.setCancelable(true);

        int width = (int) (mContext.getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (mContext.getResources().getDisplayMetrics().heightPixels * 0.30);
        dialog.getWindow().setLayout(width, lp.height);

        dialog.show();
    }

    public static void alertDialogList(final Context mContext, final ArrayList<DialogList> mDialogList,
                                       final DialogListInterface mDialogListInterface) {
        ListView mListView;
        final DialogListAdapter adapter;
        final Dialog dialog = new Dialog(mContext, R.style.AlertDialogCustom);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = LayoutInflater.from(mContext).inflate(R.layout.dialog_list, null);
        mListView = (ListView) v.findViewById(R.id.listView);

        adapter = new DialogListAdapter(mContext, mDialogList);
        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                mDialogListInterface.DialogListInterface("", mDialogList.get(position).value);
                dialog.dismiss();
            }
        });

        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialogCustom;
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.setContentView(v);
        dialog.setCancelable(true);

        int width = (int) (mContext.getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (mContext.getResources().getDisplayMetrics().heightPixels * 0.30);
        dialog.getWindow().setLayout(width, lp.height);

        dialog.show();
    }

    public static void alertGallery(final Context mContext, final View.OnClickListener cameraClick,
                                    final View.OnClickListener galleryClick, final View.OnClickListener cancelClick) {
        TextView mTxtCamera, mTxtGallery, mTxtCancel;
        final Dialog dialog = new Dialog(mContext, R.style.AlertDialogCustom);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = LayoutInflater.from(mContext).inflate(R.layout.alert_gallery, null);
        mTxtCamera = (TextView) v.findViewById(R.id.txtCamera);
        mTxtGallery = (TextView) v.findViewById(R.id.txtGallery);
        mTxtCancel = (TextView) v.findViewById(R.id.txtCancel);

        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialogCustom;
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        mTxtCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (cameraClick != null) {
                    cameraClick.onClick(v);
                }
            }
        });

        mTxtGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (galleryClick != null) {
                    galleryClick.onClick(v);
                }
            }
        });
        mTxtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (cancelClick != null) {
                    cancelClick.onClick(v);
                }
            }
        });

        dialog.setContentView(v);
        dialog.setCancelable(false);

        int width = (int) (mContext.getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (mContext.getResources().getDisplayMetrics().heightPixels * 0.30);
        dialog.getWindow().setLayout(width, lp.height);

        dialog.show();
    }

    public static void changeLanguageDialog(final Context mContext, String language, final View.OnClickListener okClick) {
        final TextView mTxtSubmit, mTxtLanguage;
        final ArrayList<DialogList> mDialogList = new ArrayList<>();
        final Dialog dialog = new Dialog(mContext, R.style.AlertDialogCustom);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = LayoutInflater.from(mContext).inflate(R.layout.alert_change_language, null);
        mTxtLanguage = (TextView) v.findViewById(R.id.txtLanguage);
        mTxtSubmit = (TextView) v.findViewById(R.id.txtSubmit);

        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialogCustom;
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        if (language.equalsIgnoreCase("")) {
            mTxtLanguage.setText("ENGLISH");
        } else {
            mTxtLanguage.setText(language);
        }

        mTxtLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialogList.clear();
                mDialogList.add(new DialogList("", "ENGLISH"));
                mDialogList.add(new DialogList("", "HINDI"));
                mDialogList.add(new DialogList("", "SPANISH"));
                PopUtils.alertDialogList(mContext, mDialogList, new DialogListInterface() {
                    @Override
                    public void DialogListInterface(String id, String value) {
                        mTxtLanguage.setText(value);
                    }
                });
            }
        });

        mTxtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (okClick != null) {
                    v.setTag(mTxtLanguage.getText().toString());
                    okClick.onClick(v);
                }
            }
        });

        dialog.setContentView(v);
        dialog.setCancelable(false);

        int width = (int) (mContext.getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (mContext.getResources().getDisplayMetrics().heightPixels * 0.30);
        dialog.getWindow().setLayout(width, lp.height);

        dialog.show();
    }

    public static String getPath(Context mContext, Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager _connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean _isConnected = false;
        NetworkInfo _activeNetwork = _connManager.getActiveNetworkInfo();
        if (_activeNetwork != null) {
            _isConnected = _activeNetwork.isConnectedOrConnecting();
        }
        return _isConnected;
    }

    public static byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
        return stream.toByteArray();
    }

    public static void SetLocalization(Context mContext, String language) {
        Locale locale = null;
        if (language.equalsIgnoreCase("ENGLISH")) {
            locale = new Locale("en");
        } else if (language.equalsIgnoreCase("HINDI")) {
            locale = new Locale("hi");
        } else if (language.equalsIgnoreCase("SPANISH")) {
            locale = new Locale("es");
        }
        Configuration config = mContext.getResources().getConfiguration();
        config.locale = locale;
        mContext.getResources().updateConfiguration(config, mContext.getResources().getDisplayMetrics());

//        https://stackoverflow.com/questions/39589841/configuration-locale-variable-is-deprecated-android
//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            setSystemLocale(config, locale);
//        } else {
//            setSystemLocaleLegacy(config, locale);
//        }
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
//            mContext.getApplicationContext().getResources().updateConfiguration(config, mContext.getResources().getDisplayMetrics());
//        }
    }

    @SuppressWarnings("deprecation")
    public Locale getSystemLocaleLegacy(Configuration config) {
        return config.locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public Locale getSystemLocale(Configuration config) {
        return config.getLocales().get(0);
    }

    @SuppressWarnings("deprecation")
    public static void setSystemLocaleLegacy(Configuration config, Locale locale) {
        config.locale = locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public static void setSystemLocale(Configuration config, Locale locale) {
        config.setLocale(locale);
    }

    public static String getRealPathFromURI(Activity activity, Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        @SuppressWarnings("deprecation")
        Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
