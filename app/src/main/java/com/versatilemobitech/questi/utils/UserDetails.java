package com.versatilemobitech.questi.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Excentd11 on 8/8/2017.
 */

public class UserDetails {
    public static final String PREF_NAME = "Questi";
    private static UserDetails instance;
    private SharedPreferences sh;
    private SharedPreferences.Editor edit;

    private UserDetails(Context mContext) {
        sh = mContext.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
        edit = sh.edit();
    }

    public static synchronized UserDetails getInstance(Context mContext) {
        if (instance == null) {
            instance = new UserDetails(mContext);
        }
        return instance;
    }

    public void clear() {
        edit.clear().commit();
    }

    public void setUserId(String userId) {
        edit.putString("user_id", userId).commit();
    }

    public void setUserEmail(String userEmail) {
        edit.putString("user_email", userEmail).commit();
    }

    public void setUserName(String userEmail) {
        edit.putString("user_name", userEmail).commit();
    }

    public void setUserProfileImage(String userEmail) {
        edit.putString("user_profile_image", userEmail).commit();
    }

    public void setUserFollowing(String userEmail) {
        edit.putString("user_following", userEmail).commit();
    }

    public void setUserFollowers(String userEmail) {
        edit.putString("user_followers", userEmail).commit();
    }

    public void setHowILookActivity(boolean howILookActivity) {
        edit.putBoolean("how_i_look_activity", howILookActivity).commit();
    }

    public void setChooseLanguageActivity(boolean ChooseLanguageActivity) {
        edit.putBoolean("choose_language_activity", ChooseLanguageActivity).commit();
    }


    public void setLanguage(String language) {
        edit.putString("language", language).commit();
    }


    public String getUserId() {
        return sh.getString("user_id", "");
    }

    public String getUserEmail() {
        return sh.getString("user_email", "");
    }

    public String getUserName() {
        return sh.getString("user_name", "");
    }

    public String getUserProfileImage() {
        return sh.getString("user_profile_image", "");
    }

    public String getUserFollowing() {
        return sh.getString("user_following", "");
    }

    public String getUserFollowers() {
        return sh.getString("user_followers", "");
    }

    public boolean getHowILookActivity() {
        return sh.getBoolean("how_i_look_activity", false);
    }

    public boolean getChooseLanguageActivity() {
        return sh.getBoolean("choose_language_activity", false);
    }

    public String getLanguage() {
        return sh.getString("language", "");
    }
}
