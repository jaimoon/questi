package com.versatilemobitech.questi.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.EditText;

import com.versatilemobitech.questi.R;


@SuppressLint("AppCompatCustomView")
public class OpenSanEditText extends EditText {

    public OpenSanEditText(Context context) {
        super(context);
    }

    public OpenSanEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs, R.attr.font_type1);
    }

    public OpenSanEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context, attrs, defStyle);
    }

    private void initView(Context context, AttributeSet attrs, int defStyle) {
        if (isInEditMode())
            return;
        try {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.OpenSanTextView);

            String str = a.getString(R.styleable.OpenSanTextView_font_type1);
            if (str == null)
                str = "1";
            a.recycle();
            switch (Integer.parseInt(str)) {
                case 1:
                    str = "OpenSans-Regular.ttf";
                    break;
                case 2:
                    str = "OpenSans-Semibold.ttf";
                    break;
                case 3:
                    str = "OpenSans-SemiboldItalic.ttf";
                    break;
                default:
                    str = "OpenSans-Regular.ttf";
                    break;
            }
            setTypeface(FontManager.getInstance(getContext()).loadFont(str));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

