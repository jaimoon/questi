package com.versatilemobitech.questi.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import com.versatilemobitech.questi.R;


public class OpenSanTextView extends TextView {
    private int font;

    public OpenSanTextView(Context context) {
        super(context);
    }

    public OpenSanTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
//         initFont(context, attrs);
        initView(context, attrs);
    }

    public OpenSanTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // initFont(context, attrs);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        try {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.OpenSanTextView);
            String str = a.getString(R.styleable.OpenSanTextView_font_type1);
            if (str == null)
                str = "1";
            switch (Integer.parseInt(str)) {
                case 1:
                    str = "OpenSans-Regular.ttf";
                    break;
                case 2:
                    str = "OpenSans-Semibold.ttf";
                    break;
                case 3:
                    str = "OpenSans-SemiboldItalic.ttf";
                    break;
                default:
                    str = "OpenSans-Regular.ttf";
                    break;
            }
            setTypeface(FontManager.getInstance(getContext()).loadFont(str));
            a.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}