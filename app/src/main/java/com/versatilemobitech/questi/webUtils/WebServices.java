package com.versatilemobitech.questi.webUtils;

/**
 * Created by CHITTURI on 07-Oct-16.
 */

public class WebServices {

    //public static String BASE_URL = "http://fansadda.mobi/questi/ws/index.php?";
    public static String BASE_URL = "https://versatilemobitech.mobi/Questi/ws/questi.php?";

    public static String URL_REGISTRATION = BASE_URL + "action=register";
    public static String URL_LOGIN = BASE_URL + "action=login";
    public static String URL_ADD_PROFILE = BASE_URL + "action=profile";
    public static String URL_FORGOT_PASSWORD = BASE_URL + "action=forgot_password";
    public static String URL_HOME = BASE_URL + "action=userquestions";
    public static String URL_EXPLORE = BASE_URL + "action=getall";
    public static String URL_ALL_USERS = BASE_URL + "action=allusers";
    public static String URL_REPORT = BASE_URL + "action=spam";
    public static String URL_GET_QUESTIONS = BASE_URL + "action=getquestions";
    public static String URL_DELETE = BASE_URL + "action=delete";
    public static String URL_FOLLOW = BASE_URL + "action=follow";
    public static String URL_ASK_QUESTION = BASE_URL + "action=question";
    public static String URL_LIKE = BASE_URL + "action=likes";
    public static String URL_POST_ANSWER = BASE_URL + "action=answer";
    public static String URL_POST_LIKED_ANSWER = BASE_URL + "action=likedanswers";
    public static String URL_EDIT_PROFILE = BASE_URL + "action=editname";
    public static String URL_FOLLOWERS = BASE_URL + "action=followerlist";
    public static String URL_FOLLOWINGS = BASE_URL + "action=followinglist";
    public static String URL_MY_PROFILE = BASE_URL + "action=myprofile";
    public static String URL_MY_NOTIFICATIONS = BASE_URL + "action=notifications";
    public static String URL_MY_CHANGE_PASSWORD = BASE_URL + "action=changepassword";

    public static String LOGIN = "https://versatilemobitech.mobi/Questi/ws/questi.php?action=user_login&login_type=";
}
