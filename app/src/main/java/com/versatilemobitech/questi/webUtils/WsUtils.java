package com.versatilemobitech.questi.webUtils;

/**
 * Created by CHITTURI on 07-Oct-16.
 */

public class WsUtils {
    public final static int WS_CODE_REGISTRATION = 101;
    public final static int WS_CODE_LOGIN = 102;
    public final static int WS_CODE_ADD_PROFILE = 103;
    public final static int WS_CODE_FORGOT_PASSWORD = 104;
    public final static int WS_CODE_HOME = 105;
    public final static int WS_CODE_REPORT = 106;
    public final static int WS_CODE_GET_QUESTIONS = 107;
    public final static int WS_CODE_EXPLORE = 108;
    public final static int WS_CODE_DELETE = 109;
    public final static int WS_CODE_All_USERS = 110;
    public final static int WS_CODE_FOLLOW = 111;
    public final static int WS_CODE_ASK_QUESTION = 112;
    public final static int WS_CODE_LIKE = 113;
    public final static int WS_CODE_POST_ANSWER = 114;
    public final static int WS_CODE_LIKED_ANSWERS = 115;
    public final static int WS_CODE_EDIT_PROFILE = 116;
    public final static int WS_CODE_FOLLOWERS = 117;
    public final static int WS_CODE_MY_PROFILE = 118;
    public final static int WS_CODE_NOTIFICATIONS = 119;
    public final static int WS_CODE_CHANGE_PASSWORD = 120;
    public final static int WS_CODE_FOLLOWINGS = 121;
    public final static int WS_CODE_FACEBOOK_LOGIN = 122;
}
